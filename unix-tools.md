# Unix Utils and Tools

* [cURL examples without web requests](https://medium.com/geekculture/5-extra-uses-for-curl-that-dont-involve-web-requests-6780a345877f)
* [httpie - alternative to cURL](https://github.com/httpie/cli)
* [EXA - alternative to ls](https://the.exa.website/)
* [LSD - alternative to ls](https://github.com/Peltoche/lsd)
* [BAT - alternative to cat](https://github.com/sharkdp/bat)
* [Delta - enhanced file diff](https://github.com/dandavison/delta)
* [RipGrep - alternative to grep](https://github.com/BurntSushi/ripgrep)
* [Procs - alternative to ps](https://github.com/dalance/procs)
* [FD - alternative to find](https://github.com/sharkdp/fd)
* [FZF Fuzzy Finder](https://github.com/junegunn/fzf)
* [Zoxide alternative to cd](https://github.com/ajeetdsouza/zoxide)


# TMUX Cheat Sheet

As per my configuration in my dotfiles

### General

#### start new session
```
tmux
```

#### start new session with a name
```
tmux new -s myname
```

#### attach to a pre-existing session
```
tmux a -t myname
```

#### list session
```
tmux ls
```

#### kill session
```
tmux kill-session -t myname
```

### Windows

#### new
```
prefix c
```

#### name
```
prefix ,
```

#### list
```
prefix w
```

#### find
```
prefix f
```

#### move (will prompt for a new number)
```
prefix .
```

#### kill (will prompt for confirmation)
```
prefix X
```

### Panes

#### create horizontal
```
prefix -
```

#### create vertical
```
prefix |
```

#### show pane number
```
prefix q
```

#### rotate
```
ctrl-o
```

#### move around
```
alt-h move to left pane of the current position
alt-j move to the pane below currentt position
alt-k move to the pane above the current position
alt-l move to the right pane of the the current position
```


#### turn on synchronization
```
prefix s
```

#### turn off synchronization
```
prefix S 
```

You can still achieve synchronization without my key mappings.
```
prefix :

then

:setw synchronize-panes on

or

:setw synchronize-panes off

```


## Reference Links
- [tmux cheatsheet.com](https://tmuxcheatsheet.com/)
- [how to reorder windows](https://superuser.com/questions/343572/how-do-i-reorder-tmux-windows)
- [tmux cheat sheet](https://gist.github.com/MohamedAlaa/2961058)



#!/bin/bash

if ! tmux has-session -t nene; then

    ## Create a tmux session, with windows and panes

    # Create a new TMUX session and detach from it
    tmux new -s nene -d

    # create a new window
    tmux new-window -t nene:2

    # split the window into 2 panes vertically
    tmux split-window -t nene:2 -v

    tmux send-keys -t nene:1 vim Enter

    tmux send-keys -t nene:2.0 \
        htop Enter

    # add environment variables to the bottom pane
    tmux send-keys -t nene:2.1 \
        export ENV_NENE=1 Enter

    # create a new window
    tmux new-window -t nene:3 \
        htop Enter

    tmux split-window -t nene:3 \
        vim ~/.zshrc Enter


#tmux split-window -v -p 30
#tmux split-window -h -p 66
#tmux split-window -h -p 50

fi

tmux attach -t nene


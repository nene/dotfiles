# ===============================================
#|_   _| __ ___  _   ___  __
#  | || '_ ` _ \| | | \ \/ /
#  | || | | | | | |_| |>  <
#  |_||_| |_| |_|\__,_/_/\_\
# ===============================================

### Unbind default prefix key
unbind C-b


### Set the prefix globally. Remap from C-b to C-Space
set-option -g prefix C-Space
bind C-Space send-prefix
bind-key C-Space send-prefix


## Decrease wait time when ESC is pressed to 20ms
set -g escape-time 20

## Set command history size in the buffer
set -g history-limit 10000
#
## Using VIM inside TMUX.
## Fix delay time between tmux an vim.
## Do not wait after hitting ESC before it responds
set -sg escape-time 0

## Make messages stay on the screen for 2 seconds
set -g display-time 1000
set-option -g display-panes-time 2000

# Super useful when using "grouped sessions" and multi-monitor setup
setw -g aggressive-resize on

## Enable mouse support
set -g mouse on

## Turn on mouse support
set-option -g -q mode-mouse on


####################
# COPY AND PASTE
####################
##unbind p
##bind p paste-buffer


####################
# WINDOWS 
####################

## Create new window
bind-key c new-window -c "#{pane_current_path}"

## Start windows at index 1 and not 0 
set -g base-index 1

## Start panes at index 1 and not 0
setw -g pane-base-index 1

## Close a window
bind X confirm kill-window

## Ensure winow index numbers get reordered on delete
set-option -g renumber-windows on

## Display pane numbers
bind-key d display-panes

## Disable window automatic rename
set -g automatic-rename off
setw -g automatic-rename off
set-window-option -g automatic-rename off

## Ensure window titles get renamed automatically
##setw -g automatic-rename

## Swap windows - change window index
## mode the current windows to be the first window
bind-key T swap-window -t 1

## Control-Shift-Left move the current window to the left position
bind-key -n C-S-Left swap-window -t -1

## Control-Shift-Left move the current window to the left position
bind-key -n C-S-Right swap-window -t +1

## Window rotation. Ommit prefix with -n
bind-key -n C-r rotate-window

## Window switching with vim keys.
## Ommit prefix with -n .In addition to prefix+index; prefix+n; prefix+p
#bind -r -n C-h select-window -t :-
#bind -r -n C-l select-window -t :+

bind -n C-p previous-window
bind -n C-n next-window


## Window Swap with arrows - NEED TO REMAP - NOT COMPATIBLE WITH NEOVIM
#bind -n S-Up swap-window -t -1       # swap window left 1
#bind -n S-Down swap-window -t +1     # swap window right 1
#bind -n S-Left select-window -t -1   # move window left 1
#bind -n S-Right select-window -t +1  # move window right 1


####################
# PANES
####################
## unbind default pane split keys
unbind '"'
unbind %

### Set key-bindings to create panes
bind-key - split-window -v -c "#{pane_current_path}"
bind-key | split-window -h -c "#{pane_current_path}"


### Set keys to move around panes. Use ALT + arrow keys
bind-key -n M-Left  select-pane -L
bind-key -n M-Down  select-pane -D
bind-key -n M-Up    select-pane -U
bind-key -n M-Right select-pane -R


## Resize Panes
bind-key -r H resize-pane -L 5
bind-key -r J resize-pane -D 5
bind-key -r K resize-pane -U 5
bind-key -r L resize-pane -R 5


####################
# SYNCRONIZE-PANES
# enables syncronize-panes toggling
# Set pane synchronization of keys. Ommit prefix with -n
#bind -n C-s setw synchronize-panes
####################

## Uppercase Turns Sync on
bind-key s setw synchronize-panes on \; display-message "Panel Sync on"

## Lowercase Turns Sync off
bind-key S setw synchronize-panes off \; display-message "Panel Sync off"

## rearrange panes
bind-key = select-layout main-horizontal
bind-key + select-layout main-vertical


bind-key : command-prompt
bind-key r refresh-client
bind-key L clear-history


####################
# Colors
####################

## Enable color support inside tmux
## Set default terminal mode to 256 colors - ensure we're using 256 colors
set-option -g default-terminal "screen-256color"
set -g default-terminal "screen-256color"

## Add truecolor support
set-option -ga terminal-overrides ",xterm-256color:Tc"


## Reload config file by the command line
bind-key R source-file ~/.tmux.conf \; display-message "TMUX Configuration Reloaded "



####################
# Plugin Support
####################

###set -g @plugin 'tmux-plugins/tmux-sensible'

### Status bar
#set -g @plugin 'egel/tmux-gruvbox'
set -g @plugin "arcticicestudio/nord-tmux"
#set -g @plugin 'dracula/tmux'

# TPM - TMUX Plugin Manager
# Initialize TPM - keep this line at the VERY BOTTON of tmux.cond
run -b '~/.tmux/plugins/tpm/tpm'


local ufo_ok, ufo = pcall(require, "ufo")

if not ufo_ok then
    return
end

local foldHandler = function(virtText, lnum, endLnum, width, truncate)
    local newVirtText = {}
    local suffix = (" 󰁂 %d "):format(endLnum - lnum)
    local sufWidth = vim.fn.strdisplaywidth(suffix)
    local targetWidth = width - sufWidth
    local curWidth = 0
    for _, chunk in ipairs(virtText) do
        local chunkText = chunk[1]
        local chunkWidth = vim.fn.strdisplaywidth(chunkText)
        if targetWidth > curWidth + chunkWidth then
            table.insert(newVirtText, chunk)
        else
            chunkText = truncate(chunkText, targetWidth - curWidth)
            local hlGroup = chunk[2]
            table.insert(newVirtText, { chunkText, hlGroup })
            chunkWidth = vim.fn.strdisplaywidth(chunkText)
            -- str width returned from truncate() may less than 2nd argument, need padding
            if curWidth + chunkWidth < targetWidth then
                suffix = suffix .. (" "):rep(targetWidth - curWidth - chunkWidth)
            end
            break
        end
        curWidth = curWidth + chunkWidth
    end
    table.insert(newVirtText, { suffix, "MoreMsg" })
    return newVirtText
end

-- buffer scope handler
-- will override global handler if it is existed
-- local bufnr = vim.api.nvim_get_current_buf()

ufo.setup({
    fold_virt_text_handler = foldHandler,
    open_fold_hl_timeout = 150,
    -- close_fold_kinds = {'imports', 'comment'},
    preview = {
        win_config = {
            border = { "", "─", "", "", "", "─", "", "" },
            winhighlight = "Normal:Folded",
            winblend = 0,
        },
        mappings = {
            scrollU = "<C-u>",
            scrollD = "<C-d>",
            jumpTop = "[",
            jumpBot = "]",
        },
    },
    provider_selector = function(bufnr, filetype)
        -- return { "lsp", "indent" }
        -- return { "lsp", "treesitter" }
        return { "treesitter", "indent" }
        -- return { "lsp", "treesitter", "indent" }
    end,
})

vim.keymap.set("n", "zR", ufo.openAllFolds, { noremap = true, silent = true, desc = "Reveal/Open all folds" })

vim.keymap.set("n", "zM", ufo.closeAllFolds, { noremap = true, silent = true, desc = "Collapse/Close all folds" })

vim.keymap.set("n", "zr", ufo.openFoldsExceptKinds, { noremap = true, silent = true })

-- closeAllFolds == closeFoldsWith(0)
vim.keymap.set("n", "zm", ufo.closeFoldsWith, { noremap = true, silent = true })

vim.keymap.set("n", "zp", function()
    local winid = ufo.peekFoldedLinesUnderCursor()
    if not winid then
        vim.lsp.buf.hover()
    end
end, { noremap = true, silent = true, desc = "Peek into a fold" })

-- [[ Configure Treesitter ]]
-- See `:help nvim-treesitter`

local ts_ok, treesitter_config = pcall(require, "nvim-treesitter.configs")
if not ts_ok then
    vim.notify("Plugin [treesitter] not loaded", vim.log.levels.WARN)
    return
end

treesitter_config.setup({
    -- Add languages to be installed here that you want installed for treesitter
    ensure_installed = {
        "go",
        "lua",
        "python",
        "rust",
        "regex",
        "typescript",
        "javascript",
        "java",
        "bash",
        "markdown",
        "markdown_inline",
        "sql",
        "toml",
        "vim",
        "vimdoc",
    },
    auto_install = true,
    synch_install = false, -- only applied to 'ensured_installed'
    highlight = {
        enable = true,
        additional_vim_regex_highlighting = false,
    },
    indent = {
        enable = true, -- false will disable the whole extension!
    },
    query_linter = {
        enable = true,
        use_virtaul_text = true,
        lint_events = { "BufWrite", "CursorHold" },
    },
    rainbow = {
        enable = true,
        extended_mode = true,
        max_file_lines = nil,
    },
    incremental_selection = {
        enable = true,
        keymaps = {
            init_selection = "gnn",
            node_incremental = "grn",
            scope_incremental = "grc",
            node_decremental = "grm",
        },
    },

    textsubjects = {
        enable = true,
        prev_selection = "~", -- (Optional) keymap to select the previous selection
        keymaps = {
            ["."] = "textsubjects-smart",
            ["S-Up"] = "textsubjects-container-outer",
            ["S-Down"] = "textsubjects-container-inner",
        },
    },

    textobjects = {
        lsp_interop = {
            enable = true,
            border = "none",
            peek_definition_code = {
                ["<leader>pf"] = "@function.outer",
                ["<leader>pc"] = "@class.outer",
            },
        },
        select = {
            enable = true,
            lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
            -- You can use the capture groups defined in textobjects.scm
            keymaps = {
                ["aa"] = "@parameter.outer",
                ["ia"] = "@parameter.inner",
                ["af"] = "@function.outer",
                ["if"] = "@function.inner",
                ["ac"] = "@class.outer",
                ["ic"] = "@class.inner",
                ["ii"] = "@conditional.inner",
                ["ai"] = "@conditional.outer",
                ["il"] = "@loop.inner",
                ["al"] = "@loop.outer",
                ["at"] = "@comment.outer",
            },
            selection_modes = {
                ["@parameter.outer"] = "v", --charwise
                ["@function.outer"] = "V", --linewise
                ["@class.outer"] = "<c-v>", --blockwise
            },
        },
        move = {
            enable = true,
            set_jumps = true, -- whether to set jumps in the jumplist
            goto_next_start = {
                ["]m"] = "@function.outer",
                ["]]"] = "@class.outer",
            },
            goto_next_end = {
                ["]M"] = "@function.outer",
                ["]["] = "@class.outer",
            },
            goto_previous_start = {
                ["[m"] = "@function.outer",
                ["[["] = "@class.outer",
            },
            goto_previous_end = {
                ["[M"] = "@function.outer",
                ["[]"] = "@class.outer",
            },
            goto_next = {
                ["]i"] = "@conditional.inner",
            },
            goto_previous = {
                ["[i"] = "@conditional.inner",
            },
        },
        swap = {
            enable = true,
            swap_next = {
                ["<leader>xp"] = "@parameter.inner",
            },
            swap_previous = {
                ["<leader>xP"] = "@parameter.inner",
            },
        },
    },
})

--vim.opt.foldlevel = 20
--vim.opt.foldmethod = "expr"
--vim.opt.foldexpr = "nvim_treesitter#foldexpr()"

local ts_ctx_ok, treesitter_ctx = pcall(require, "treesitter-context")
if not ts_ctx_ok then
    vim.notify("Plugin [treesitter-context] not loaded", vim.log.levels.WARN)
    return
end

treesitter_ctx.setup({
    enable = true,         -- Enable this plugin (Can be enabled/disabled later via commands)
    max_lines = 0,         -- How many lines the window should span. Values <= 0 mean no limit.
    min_window_height = 0, -- Minimum editor window height to enable context. Values <= 0 mean no limit.
    line_numbers = true,
    multiline_threshold = 20, -- Maximum number of lines to show for a single context
    trim_scope = "outer",  -- Which context lines to discard if `max_lines` is exceeded. Choices: 'inner', 'outer'
    mode = "cursor",       -- Line used to calculate context. Choices: 'cursor', 'topline'
    -- Separator between context and content. Should be a single character string, like '-'.
    -- When separator is set, the context will only show up when there are at least 2 lines above cursorline.
    separator = nil,
    zindex = 20,  -- The Z-index of the context window
    on_attach = nil, -- (fun(buf: integer): boolean) return false to disable attaching
})

vim.keymap.set("n", "[c", function()
    require("treesitter-context").go_to_context(vim.v.count1)
end, { silent = true })

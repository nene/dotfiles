local cmp = require("cmp")
local compare = require("cmp.config.compare")
local lsp_zero = require("lsp-zero")
local lspkind = require("lspkind")
local luasnip = require("luasnip")

local source_mapping = {
    nvim_lsp = "[Lsp]",
    luasnip = "[Snip]",
    buffer = "[Buffer]",
    nvim_lua = "[Lua]",
    treesitter = "[Tree]",
    path = "[Path]",
    rg = "[Rg]",
    nvim_lsp_signature_help = "[Sig]",
}

local cmp_action = lsp_zero.cmp_action()
local cmp_select_opts = { behavior = cmp.SelectBehavior.Select }

lsp_zero.extend_cmp()

cmp.setup({
    window = {
        -- completion = cmp.config.window.bordered(),
        -- documentation = cmp.config.window.bordered(),
    },

    sources = cmp.config.sources({
        { name = "nvim_lsp" },
        { name = "luasnip" },   -- snippets
        { name = "render-markdown" }, -- markdown support
        -- { name = 'ultisnips' }, -- for ultisnips users.
    }, {
        { name = "buffer" }, -- text within current buffer
        { name = "path" }, -- file system paths
    }),

    snippet = {
        expand = function(args)
            luasnip.lsp_expand(args.body)
        end,
    },

    mapping = cmp.mapping.preset.insert({
        ["<C-a>"] = cmp.mapping.abort(),
        ["<C-c>"] = cmp.mapping.close(),
        ["<C-p>"] = cmp.mapping.select_prev_item(),
        ["<C-n>"] = cmp.mapping.select_next_item(),
        ["<C-u>"] = cmp.mapping.scroll_docs(-4),
        ["<C-d>"] = cmp.mapping.scroll_docs(4),
        ["<C-f>"] = cmp_action.luasnip_jump_forward(),
        ["<C-b>"] = cmp_action.luasnip_jump_backward(),
        ["<Up>"] = cmp.mapping.select_prev_item(cmp_select_opts),
        ["<Down>"] = cmp.mapping.select_next_item(cmp_select_opts),
        ["<C-Space>"] = cmp.mapping.complete(),
        ["<CR>"] = cmp.mapping.confirm({ select = true, behavior = cmp.ConfirmBehavior.Replace }),
        ["<Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_next_item()
            elseif luasnip.expand_or_jumpable() then
                luasnip.expand_or_jump()
            else
                fallback()
            end
        end, { "i", "s" }),
        ["<S-Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_prev_item()
            elseif luasnip.expand_or_jumpable(-1) then
                luasnip.expand_or_jump(-1)
            else
                fallback()
            end
        end, { "i", "s" }),
        cmp.setup.cmdline({ "/", "?" }, {
            mapping = cmp.mapping.preset.cmdline(),
            sources = { { name = "buffer" } },
        }),

        cmp.setup.cmdline(":", {
            mapping = cmp.mapping.preset.cmdline(),
            sources = cmp.config.sources({ { name = "path" }, { name = "cmdline" } }),
        }),
    }),

    sorting = {
        priority_weight = 2,
        comparators = {
            compare.score,
            compare.recently_used,
            compare.offset,
            compare.exact,
            compare.kind,
            compare.sort_text,
            compare.length,
            compare.order,
        },
    },

    -- formatting = lsp_zero.cmp_format(),

    --    formatting = {
    --      fields = {'menu', 'abbr', 'kind'},
    --      format = lspkind.cmp_format({
    --        maxwidth = 200,
    --        ellipsis_char = '...',
    --        before = function(entry, item)
    --          local menu_icon = {
    --            nvim_lsp = 'λ',
    --            luasnip = '✎',
    --            path = 'Ψ',
    --            emoji = '🤌',
    --            nvim_lua = 'Π',
    --            calc = 'Σ',
    --            buffer = 'Ω',
    --            cmdline = '⋗',
    --          }
    --
    --          --item = formatForTailwindCSS(entry, item)
    --          item.menu = menu_icon[entry.source.name] or entry.source.name
    --          return item
    --        end
    --      })
    --    },

    formatting = {
        fields = { "kind", "abbr", "menu" },
        format = lspkind.cmp_format({
            maxwidth = 200,
            -- defines how annotations are shown
            -- default: symbol
            -- options: 'text', 'text_symbol', 'symbol_text', 'symbol'
            mode = "symbol_text",

            before = function(entry, item)
                item.kind = lspkind.presets.default[item.kind]
                local menu = source_mapping[entry.source_name]
                item.menu = menu
                return item
            end,
        }),
    },

    enabled = function()
        -- disable completion in comments
        local context = require("cmp.config.context")

        -- keep command mode completion enabled when cursor is in a comment
        if vim.api.nvim_get_mode().mode == "c" then
            return true
        else
            return not context.in_treesitter_capture("comment") and not context.in_syntax_group("Comment")
        end
    end,
})

local bufferline_ok, bufferline = pcall(require, "bufferline")

if not bufferline_ok then
    vim.notify("Plugin [bufferline] not loaded", vim.log.levels.WARN)
    print("bufferline?", vim.inspect(bufferline))
    return
end

local icons = require("base.icons")

--bufferline.setup{}

bufferline.setup({
    options = {
        numbers = "none",              -- | "none" | "ordinal" | "buffer_id" | "both" | function({ ordinal, id, lower, raise }): string,
        close_command = "Bdelete! %d", -- can be a string | function, see "Mouse actions"
        right_mouse_command = "Bdelete! %d", -- can be a string | function, see "Mouse actions"
        left_mouse_command = "buffer %d", -- can be a string | function, see "Mouse actions"
        middle_mouse_command = nil,    -- can be a string | function, see "Mouse actions"
        minimum_padding = 1,
        maximum_padding = 5,
        maximum_length = 15,
        -- NOTE: this plugin is designed with this icon in mind,
        -- and so changing this is NOT recommended, this is intended
        -- as an escape hatch for people who cannot bear it for whatever reason
        indicator = {
            icon = "▎",
        },
        icon_pinned = icons.misc.Pin,
        buffer_close_icon = icons.ui_icons.ThickX,
        modified_icon = icons.dots.SolidDot,
        close_icon = icons.ui_icons.ThickX,
        left_trunc_marker = icons.arrows.ArrowCircleLeft,
        right_trunc_marker = icons.arrows.ArrowCircleRight,
        max_name_length = 30,
        max_prefix_length = 30, -- prefix used when a buffer is de-duplicated
        tab_size = 21,
        diagnostics = "nvim_lsp", -- | "nvim_lsp" | "coc", false
        diagnostics_update_in_insert = false,
        color_icons = true,
        show_buffer_icons = true,
        show_buffer_close_icons = true,
        show_close_icon = true,
        show_tab_indicators = true,
        persist_buffer_sort = true, -- whether or not custom sorted buffers should persist
        separator_style = "thick", -- | "thick" | "thin" | "slant" | "padded_slant" |{ 'any', 'any' },
        enforce_regular_tabs = true,
        always_show_bufferline = true,
        offsets = {
            {
                filetype = "NvimTree",
                -- display working dir
                text = function()
                    local full_path = vim.fn.getcwd()
                    local current_folder = vim.fn.fnamemodify(full_path, ":t")
                    return current_folder
                end,
                highlight = "Directory",
                text_align = "left",
                -- padding = 1,
                separator = true,
            },
        },
        hover = {
            enabled = true,
            delay = 200,
            reveal = { "close" },
        },
    },
    highlights = {
        buffer_selected = {
            bold = false,
            italic = false,
            fg = "#ea4435",
        },
        fill = {
            ctermbg = "#24283b",
            bg = "#24283b",
        },
    },
})

-- clear TAB mapping before using it
vim.api.nvim_set_keymap("", "<TAB>", "<Nop>", { noremap = true, silent = true })

-- Move to previous/next
vim.keymap.set("n", "<TAB>", "<cmd>BufferLineCycleNext<cr>", { noremap = true, silent = true })
vim.keymap.set("n", "<S-TAB>", "<cmd>BufferLineCyclePrev<cr>", { noremap = true, silent = true })

-- vim.keymap.set("n", "<S-l>", "<cmd>BufferLineCycleNext<cr>", { noremap = true, silent = true })
-- vim.keymap.set("n", "<S-h>", "<cmd>BufferLineCyclePrev<cr>", { noremap = true, silent = true })

vim.keymap.set("n", "<leader><right>", "<cmd>BufferLineMoveNext<cr>", { noremap = true, silent = true })
vim.keymap.set("n", "<leader><left>", "<cmd>BufferLineMovePrev<cr>", { noremap = true, silent = true })

--vim.keymap.set("n", "<cmd>BufferLineSortByExtension<cr>", { noremap = true, silent = true })
--vim.keymap.set("n", "<cmd>BufferLineSortByDirectory<cr>", { noremap = true, silent = true })

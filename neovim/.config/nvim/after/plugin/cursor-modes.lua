local cursor_modes_ok, cursor_modes = pcall(require, "moody")
-- local cursor_modes_ok, cursor_modes = pcall(require, "modes")

if not cursor_modes_ok then
    -- vim.notify("Plugin [cursor] not loaded", vim.log.levels.WARN)
    return
end

-- cursor_modes.setup({
--     colors = {
--         bg = "", -- Optional bg param, defaults to Normal hl group
--         copy = "#f5c359",
--         delete = "#c75c6a",
--         insert = "#78ccc5",
--         visual = "#9745be",
--     },
--
--     -- Set opacity for cursorline and number background
--     line_opacity = 0.15,
--
--     -- Enable cursor highlights
--     set_cursor = true,
--
--     -- Enable cursorline initially, and disable cursorline for inactive windows
--     -- or ignored filetypes
--     set_cursorline = true,
--
--     -- Enable line number highlights to match cursorline
--     set_number = true,
--
--     -- Disable modes highlights in specified filetypes
--     -- Please PR commonly ignored filetypes
--     ignore_filetypes = { "NvimTree", "TelescopePrompt" },
-- })

-- cursor_modes.setup({
--     blend = {
--         normal = 0.2,
--         insert = 0.2,
--         visual = 0.2,
--         command = 0.2,
--         replace = 0.2,
--         select = 0.2,
--         terminal = 0.2,
--         terminal_n = 0.2,
--     },
--     disabled_buffers = {},
--     bold_nr = false,
--     NormalMoody = { fg = "#f5c359" },
--     InsertMoody = { fg = "#78ccc5" },
--     VisualMoody = { fg = "#9745be" },
--     CommandMoody = { fg = "#f5c359" },
--     ReplaceMoody = { fg = "#f5c359" },
--     SelectMoody = { fg = "#9745be" },
--     TerminalMoody = { fg = "#f5c359" },
--     TerminalNormalMoody = { fg = "#f5c359" },
-- })

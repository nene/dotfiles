local neogit_ok, neogit = pcall(require, "neogit")

if not neogit_ok then
    -- vim.notify("Plugin [neogit] failed to load", vim.log.levels.WARN)
    return
end

-- https://github.com/NeogitOrg/neogit

neogit.setup({})

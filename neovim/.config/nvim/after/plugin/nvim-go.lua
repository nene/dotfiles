local nvim_go_ok, nvim_go = pcall(require, "go")

if not nvim_go_ok then
    vim.notify("Plugin [nvim-go] not loaded", vim.log.levels.WARN)
    return
end

nvim_go.setup({})

-- nvim-lsp support goimport by default
-- autocmd BufWritePre (InsertLeave?) <buffer> lua vim.lsp.buf.formatting_sync(nil,500)

-- require("go.format").gofmt()
-- require("go.format").goimport()

-- Format on save. Run gofmt on save
-- vim.api.nvim_create_autocmd("BufWritePre", {
--     group = vim.api.nvim_create_augroup("GoFormat", { clear = true }),
--     pattern = "*.go",
--     callback = function()
--         require("go.format").gofmt()
--     end,
-- })

-- Run gofmt + goimport on save
-- vim.api.nvim_create_autocmd("BufWritePre", {
--     group = vim.api.nvim_create_augroup("GoFormat", { clear = true }),
--     pattern = { "go" },
--     callback = function()
--         require("go.format").goimport()
--     end,
-- })

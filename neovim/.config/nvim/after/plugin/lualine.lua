-- Eviline config for lualine
-- Author: shadmansaleh
-- Credit: glepnir

-- use a protected call so we don't error out on first use
local lualine_ok, lualine = pcall(require, "lualine")
if not lualine_ok then
    vim.notify("Plugin [lualine] not loaded", vim.log.levels.WARN)
    return
end

if vim.version().minor < 5 then
    vim.notify("Plugin [lualine] requires 0.5 or higher", vim.log.levels.WARN)
    return
end

-- local gps_ok, gps = pcall(require, "nvim-gps")
-- if not gps_ok then
--     vim.notify("Plugin [nvim-gps] failed to load", vim.log.levels.WARN)
--     return
-- end

-- ==========================================================================

lualine.setup({
    options = {
        icons_enabled = true,
        theme = "catppuccin",
        component_separators = "|",
        section_separators = "",
        disabled_filetypes = { "NvimTree" },
    },
})

-- ==========================================================================
local icons = require("base.icons")

local hide_in_width = function()
    return vim.fn.winwidth(0) > 80
end

local diagnostics = {
    "diagnostics",
    sources = { "nvim_diagnostic" },
    sections = { "error", "warn" },
    symbols = { error = icons.diagnostic_big.Error .. " ", warn = icons.diagnostic_big.Warning .. " " },
    colored = false,
    update_in_insert = false,
    always_visible = true,
}

local diff = {
    "diff",
    colored = false,
    symbols = {
        added = icons.git_big.Add .. " ",
        modified = icons.git_big.Mod .. " ",
        removed = icons.git_big.Remove .. " ",
        renamed = icons.git_big.Rename .. " ",
    },
    cond = hide_in_width,
}

local mode = {
    "mode",
    fmt = function(str)
        -- return "-- " .. str .. " --"
        return str
    end,
}

local filetype = {
    "filetype",
    icons_enabled = true,
    -- icon = nil,
}

local branch = {
    "branch",
    icons_enabled = true,
    icon = icons.git_small.Branch,
}

local location = {
    "location",
    padding = 0,
}

local get_progress = function()
    local current_line = vim.fn.line(".")
    local total_lines = vim.fn.line("$")
    local chars = { "__", "▁▁", "▂▂", "▃▃", "▄▄", "▅▅", "▆▆", "▇▇", "██" }
    local line_ratio = current_line / total_lines
    local index = math.ceil(line_ratio * #chars)
    return chars[index]
end

-- stylua: ignore
local colors = {
    blue   = "#80a0ff",
    cyan   = "#79dac8",
    black  = "#080808",
    white  = "#c6c6c6",
    red    = "#ff5189",
    violet = "#d183e8",
    grey   = "#303030",
}

local my_theme = {
    normal = {
        a = { fg = colors.black, bg = colors.violet },
        b = { fg = colors.white, bg = colors.grey },
        c = { fg = colors.black, bg = colors.black },
    },

    insert = { a = { fg = colors.black, bg = colors.blue } },
    visual = { a = { fg = colors.black, bg = colors.cyan } },
    replace = { a = { fg = colors.black, bg = colors.red } },

    inactive = {
        a = { fg = colors.white, bg = colors.black },
        b = { fg = colors.white, bg = colors.black },
        c = { fg = colors.black, bg = colors.black },
    },
}

-- ==========================================================================

-- lualine.setup {
--     options = {
--         icons_enabled = true,
--         theme = "tokyonight",
--         component_separators = '|',
--         -- section_separators = '',
--         section_separators = { left = '', right = '' },
--         disabled_filetypes = { "NvimTree" },
--         always_divide_middle = true,
--     },
--     sections = {
--         lualine_a = { mode },
--         lualine_b = { branch, diff, diagnostics },
--         lualine_c = {
--             { "filename" },
--             -- {
--             --     gps.get_location,
--             --     cond = gps.is_available,
--             --     color = { fg = "#f3ca28" },
--             -- },
--         },
--         lualine_x = { diff, filetype },
--         lualine_y = { location },
--         lualine_z = { get_progress },
--     },
--     inactive_sections = {
--         lualine_a = { "mode" },
--         lualine_b = { "filename" },
--         lualine_c = {},
--         lualine_x = {},
--         lualine_y = {},
--         lualine_z = { "location" },
--     },
--     tabline = {},
--     extensions = {},
-- }

local gitsigns_ok, gitsigns = pcall(require, "gitsigns")

if not gitsigns_ok then
    vim.notify("Plugin [gitsigns] not loaded", vim.log.levels.WARN)
    return
end

gitsigns.setup({})

-- Gitsigns keymappings
vim.keymap.set(
    "n",
    "<leader>gp",
    "<cmd>Gitsigns preview_hunk<cr>",
    { noremap = true, silent = true, desc = "Git Hunk Preview" }
)

vim.keymap.set(
    "n",
    "<leader>gtb",
    "<cmd>Gitsigns toggle_current_line_blame<cr>",
    { noremap = true, silent = true, desc = "Git current line blame" }
)

vim.keymap.set(
    "n",
    "<leader>gbl",
    "<cmd>Gitsigns blame_line<cr>",
    { noremap = true, silent = true, desc = "Git current line popup blame" }
)

-- Fugitive keymappings
vim.keymap.set("n", "<leader>gbf", "<cmd>Git blame<cr>", { noremap = true, silent = true, desc = "Git blame history" })

-- Telescope Git mappings
local tele_ok, _ = pcall(require, "telescope")

if not tele_ok then
    vim.notify("Plugin [telescope] failed to load", vim.log.levels.WARN)
    return
end

local builtin = require("telescope.builtin")
-- list git commits with diff preview
-- checkout action <cr>,
-- reset mixed <C-r>m,
-- reset soft <C-r>s and
-- reset hard <C-r>h
vim.keymap.set("n", "<leader>fc", builtin.git_commits, { noremap = true, silent = true, desc = "Search Git commits" })

vim.keymap.set(
    "n",
    "<leader>fbc",
    builtin.git_bcommits,
    { noremap = true, silent = true, desc = "Search Git commits for buffer" }
)

-- Lists all branches with log preview,
-- checkout action <cr>,
-- track action <C-t>,
-- rebase action<C-r>,
-- create action <C-a>,
-- switch action <C-s>,
-- delete action <C-d> and
-- merge action <C-y>
vim.keymap.set("n", "<leader>fr", builtin.git_branches, { noremap = true, silent = true, desc = "Search Git branches" })

-- Lists current changes per file with diff preview and add action. (Multi-selection still WIP)
vim.keymap.set("n", "<leader>fs", builtin.git_status, { noremap = true, silent = true, desc = "Search Git status" })

-- Lists stash items in current repository with ability to apply them on <cr>
vim.keymap.set("n", "<leader>ft", builtin.git_stash, { noremap = true, silent = true, desc = "Search Git stash" })

local harpoon_ok, harpoon = pcall(require, "harpoon")
if not harpoon_ok then
    vim.notify("Plugin [harpoon] not loaded", vim.log.levels.WARN)
    return
end

-- REQUIRED
harpoon:setup()
-- REQUIRED

vim.keymap.set("n", "<leader>ht", function()
    harpoon.ui:toggle_quick_menu(harpoon:list())
end, { noremap = true, silent = true, desc = "Harpoon List" })

vim.keymap.set("n", "<leader>ha", function()
    harpoon:list():add()
end, { noremap = true, silent = true, desc = "Harpoon Add File" })

vim.keymap.set("n", "<leader>hr", function()
    harpoon:list():remove()
end, { noremap = true, silent = true, desc = "Harpoon Remove File" })

vim.keymap.set("n", "<leader>1", function()
    harpoon:list():select(1)
end, { noremap = true, silent = true, desc = "Harpoon File 1" })

vim.keymap.set("n", "<leader>2", function()
    harpoon:list():select(2)
end, { noremap = true, silent = true, desc = "Harpoon File 2" })

vim.keymap.set("n", "<leader>3", function()
    harpoon:list():select(3)
end, { noremap = true, silent = true, desc = "Harpoon File 3" })

vim.keymap.set("n", "<leader>4", function()
    harpoon:list():select(4)
end, { noremap = true, silent = true, desc = "Harpoon File 4" })

vim.keymap.set("n", "<leader>5", function()
    harpoon:list():select(5)
end, { noremap = true, silent = true, desc = "Harpoon File 5" })

-- Toggle previous & next buffers stored within Harpoon list
vim.keymap.set("n", "<C-S-Left>", function()
    harpoon:list():prev()
end)

vim.keymap.set("n", "<C-S-Right>", function()
    harpoon:list():next()
end)

local tele_ok, tele = pcall(require, "telescope")
if not tele_ok then
    return
end

tele.load_extension("harpoon")

-- basic telescope configuration
local conf = require("telescope.config").values
local function toggle_telescope(harpoon_files)
    local file_paths = {}
    for _, item in ipairs(harpoon_files.items) do
        table.insert(file_paths, item.value)
    end

    local make_finder = function()
        local paths = {}

        for _, item in ipairs(harpoon_files.items) do
            table.insert(paths, item.value)
        end

        return require("telescope.finders").new_table({
            results = paths,
        })
    end

    require("telescope.pickers")
        .new({}, {
            prompt_title = "Harpoon",
            finder = require("telescope.finders").new_table({
                results = file_paths,
            }),
            previewer = conf.file_previewer({}),
            sorter = conf.generic_sorter({}),

            -- layout_strategy = "center",
            -- layout_config = {
            --     preview_cutoff = 1,
            --     width = function(_, max_columns, _)
            --         return math.min(max_columns, 80)
            --     end,
            --     height = function(_, _, max_lines)
            --         return math.min(max_lines, 15)
            --     end,
            -- },
            borderchars = {
                prompt = { "─", "│", " ", "│", "╭", "╮", "│", "│" },
                results = { "─", "│", "─", "│", "├", "┤", "╯", "╰" },
                preview = { "─", "│", "─", "│", "╭", "╮", "╯", "╰" },
            },
            attach_mappings = function(prompt_buffer_number, map)
                -- The keymap you need
                map("i", "<leader>hd", function()
                    local state = require("telescope.actions.state")
                    local selected_entry = state.get_selected_entry()
                    local current_picker = state.get_current_picker(prompt_buffer_number)

                    -- This is the line you need to remove the entry
                    harpoon:list():remove(selected_entry)
                    current_picker:refresh(make_finder())
                end)

                return true
            end,
        })
        :find()
end

vim.keymap.set("n", "<leader>th", function()
    toggle_telescope(harpoon:list())
end, { noremap = true, silent = true, desc = "Open harpoon window" })

local trouble_ok, trouble = pcall(require, "trouble")

if not trouble_ok then
    vim.notify("Plugin [trouble] not loaded", vim.log.levels.WARN)
    return
end

vim.keymap.set("n", "<leader>xx", function()
    trouble.toggle()
end, { noremap = true, silent = true, desc = "LSP Diagnostics" })

vim.keymap.set("n", "<leader>xw", function()
    trouble.toggle("workspace_diagnostics")
end, { noremap = true, silent = true, desc = "Workspace Diagnostics" })

vim.keymap.set("n", "<leader>xd", function()
    trouble.toggle("document_diagnostics")
end, { noremap = true, silent = true, desc = "Document Diagnostics" })

vim.keymap.set("n", "<leader>xq", function()
    trouble.toggle("quickfix")
end, { noremap = true, silent = true, desc = "Quickfix" })

vim.keymap.set("n", "<leader>xl", function()
    trouble.toggle("loclist")
end, { noremap = true, silent = true, desc = "Loc List" })

vim.keymap.set("n", "gR", function()
    trouble.toggle("lsp_references")
end, { noremap = true, silent = true, desc = "LSP References" })

local tele_ok, telescope = pcall(require, "telescope")

if not tele_ok then
    return
end

vim.keymap.set(
    "n",
    "<leader>ld",
    "<cmd>TroubleToggle document_diagnostics<cr>",
    { noremap = true, silent = true, desc = "Document Diagnostics" }
)
vim.keymap.set(
    "n",
    "<leader>lD",
    "<cmd>TroubleToggle workspace_diagnostics<cr>",
    { noremap = true, silent = true, desc = "Workspace Diagnostics" }
)

local eagle_ok, eagle = pcall(require, "eagle")

if not eagle_ok then
    -- vim.notify("Plugin [eagle] not loaded", vim.log.levels.WARN)
    return
end

eagle.setup({
    -- override the default values found in config.lua
})

-- make sure mousemoveevent is enabled
vim.o.mousemoveevent = true

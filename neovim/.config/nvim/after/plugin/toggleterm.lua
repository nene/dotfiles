local toggleterm_ok, toggleterm = pcall(require, "toggleterm")

if not toggleterm_ok then
    vim.notify("Plugin [toggleterm] not loaded", vim.log.levels.WARN)
    return
end

toggleterm.setup({
    size = 20,
    open_mapping = [[<c-\>]],
    hide_numbers = true,
    shade_filetypes = {},
    shade_terminals = true,
    shading_factor = 2,
    start_in_insert = true,
    insert_mappings = true,
    persist_size = true,
    direction = "horizontal", -- horizontal | vertical
    -- direction = "float", -- horizontal | vertical
    close_on_exit = true,
    shell = vim.o.shell,
    float_opts = {
        border = "single",
        winblend = 0,
        title_pos = "center",
        highlights = {
            border = "Normal",
            background = "Normal",
        },
    },
})

function _G.set_terminal_keymaps()
    local opts = { noremap = true }
    vim.api.nvim_buf_set_keymap(0, "t", "<esc>", [[<C-\><C-n>]], opts)
    vim.api.nvim_buf_set_keymap(0, "t", "<C-h>", [[<C-\><C-n><C-W>h]], opts)
    vim.api.nvim_buf_set_keymap(0, "t", "<C-j>", [[<C-\><C-n><C-W>j]], opts)
    vim.api.nvim_buf_set_keymap(0, "t", "<C-k>", [[<C-\><C-n><C-W>k]], opts)
    vim.api.nvim_buf_set_keymap(0, "t", "<C-l>", [[<C-\><C-n><C-W>l]], opts)
end

-- vim.cmd("autocmd! TermOpen term://* lua set_terminal_keymaps()")
-- Terminal 1: Bottom horizontal part of the screen
vim.api.nvim_set_keymap(
    "n",
    "<M-1>",
    "<Cmd>1ToggleTerm size=8 direction=horizontal<CR>",
    { noremap = true, silent = true }
)
vim.api.nvim_set_keymap(
    "t",
    "<M-1>",
    "<Cmd>1ToggleTerm size=8 direction=horizontal<CR>",
    { noremap = true, silent = true }
)

-- Terminal 2: Right vertical part of the screen
vim.api.nvim_set_keymap(
    "n",
    "<M-2>",
    "<Cmd>2ToggleTerm size=40 direction=vertical<CR>",
    { noremap = true, silent = true }
)
vim.api.nvim_set_keymap(
    "t",
    "<M-2>",
    "<Cmd>2ToggleTerm size=40 direction=vertical<CR>",
    { noremap = true, silent = true }
)

-- Terminal 3: Floating terminal in the middle of the screen
vim.api.nvim_set_keymap("n", "<M-3>", "<Cmd>3ToggleTerm size=80 direction=float<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("t", "<M-3>", "<Cmd>3ToggleTerm size=80 direction=float<CR>", { noremap = true, silent = true })

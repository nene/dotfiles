vim.keymap.set(
    "n",
    "<leader>ut",
    vim.cmd.UndotreeToggle,
    { noremap = true, silent = true, desc = "Mark file with Harpoon" }
)

require("nvim-web-devicons").set_icon({
    zsh = {
        icon = "",
        color = "#428850",
        cterm_color = "65",
        name = "Zsh",
    },
})

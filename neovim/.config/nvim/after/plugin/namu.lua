local namu_ok, namu = pcall(require, "namu")

if not namu_ok then
    -- vim.notify("Plugin [namu] not loaded", vim.log.levels.WARN)
    return
end

namu.setup({
    -- Enable the modules you want
    namu_symbols = {
        enable = true,
        options = {}, -- here you can configure namu
    },
    -- Optional: Enable other modules if needed
    colorscheme = {
        enable = false,
        options = {
            -- NOTE: if you activate persist, then please remove any vim.cmd("colorscheme ...") in your config, no needed anymore
            persist = true, -- very efficient mechanism to Remember selected colorscheme
            write_shada = false, -- If you open multiple nvim instances, then probably you need to enable this
        },
    },
    ui_select = { enable = false }, -- vim.ui.select() wrapper
})

-- === Suggested Keymaps: ===
local namu_symbols = require("namu.namu_symbols")
vim.keymap.set("n", "<leader>ns", namu_symbols.show, { desc = "Jump to LSP symbol", silent = true })

local namu_colorscheme = require("namu.colorscheme")
vim.keymap.set("n", "<leader>th", namu_colorscheme.show, { desc = "Colorscheme Picker", silent = true })

-- keymaps reference: https://github.com/bassamsdata/namu.nvim?tab=readme-ov-file#navigation-keymaps
-- custom actions: https://github.com/bassamsdata/namu.nvim?tab=readme-ov-file#custom-actions

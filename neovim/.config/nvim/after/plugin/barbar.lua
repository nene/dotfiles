local barbar_ok, barbar = pcall(require, "barbar")

if not barbar_ok then
    -- vim.notify("Plugin [barbar] not loaded", vim.log.levels.WARN)
    return
end

-- vim.g.barbar_auto_setup = false -- disable auto-setup

local icons = require("base.icons")

barbar.setup({
    --  -- WARN: do not copy everything below into your config!
    --  --       It is just an example of what configuration options there are.
    --  --       The defaults are suitable for most people.
    --
    -- Enable/disable animations
    animation = true,

    -- Automatically hide the tabline when there are this many buffers left.
    -- Set to any value >=0 to enable.
    auto_hide = false,

    --  -- Enable/disable current/total tabpages indicator (top right corner)
    --  tabpages = true,
    --
    -- Enables/disable clickable tabs
    --  - left-click: go to buffer
    --  - middle-click: delete buffer
    clickable = true,
    --
    --  -- Excludes buffers from the tabline
    --  exclude_ft = {'javascript'},
    --  exclude_name = {'package.json'},
    --
    --  -- A buffer to this direction will be focused (if it exists) when closing the current buffer.
    --  -- Valid options are 'left' (the default), 'previous', and 'right'
    --  focus_on_close = 'left',
    --
    --  -- Hide inactive buffers and file extensions. Other options are `alternate`, `current`, and `visible`.
    --  hide = {extensions = true, inactive = true},
    --
    --  -- Disable highlighting alternate buffers
    --  highlight_alternate = false,
    --
    --  -- Disable highlighting file icons in inactive buffers
    --  highlight_inactive_file_icons = false,
    --
    --  -- Enable highlighting visible buffers
    highlight_visible = true,
    --
    icons = {
        -- Configure the base icons on the bufferline.
        -- Valid options to display the buffer index and -number are `true`, 'superscript' and 'subscript'
        buffer_index = true,
        buffer_number = false,
        button = icons.ui_small.ThickX,
        -- Enables / disables diagnostic symbols
        diagnostics = {
            [vim.diagnostic.severity.ERROR] = { enabled = true, icon = icons.diagnostic_small.BoldError },
            [vim.diagnostic.severity.WARN] = { enabled = true, icon = icons.diagnostic_small.BoldWarning },
            [vim.diagnostic.severity.INFO] = { enabled = true, icon = icons.diagnostic_small.BoldInformation },
            [vim.diagnostic.severity.HINT] = { enabled = true, icon = icons.diagnostic_small.BoldHint },
        },
        gitsigns = {
            added = { enabled = true, icon = icons.diff.added },
            changed = { enabled = true, icon = icons.diff.modified },
            deleted = { enabled = true, icon = icons.diff.removed },
        },
        filetype = {
            -- Sets the icon's highlight group. If false, will use nvim-web-devicons colors
            custom_colors = false,
            -- Requires `nvim-web-devicons` if `true`
            enabled = true,
        },
        --    separator = {left = '▎', right = ''},
        -- If true, add an additional separator at the end of the buffer list
        separator_at_end = true,
        -- Configure the icons on the bufferline when modified or pinned.
        -- Supports all the base icon options.
        modified = { button = icons.ui_small.SolidDot },
        pinned = { button = icons.misc.Pin, filename = true },
        --
        --    -- Use a preconfigured buffer appearance— can be 'default', 'powerline', or 'slanted'
        --    preset = 'default',
        --
        --    -- Configure the icons on the bufferline based on the visibility of a buffer.
        --    -- Supports all the base icon options, plus `modified` and `pinned`.
        --    alternate = {filetype = {enabled = false}},
        --    current = {buffer_index = true},
        --    inactive = {button = '×'},
        --    visible = {modified = {buffer_number = false}},
    },

    --  -- If true, new buffers will be inserted at the start/end of the list.
    --  -- Default is to insert after current buffer.
    insert_at_end = false,
    insert_at_start = false,

    -- Sets the maximum padding width with which to surround each tab
    maximum_padding = 1,
    -- Sets the minimum padding width with which to surround each tab
    minimum_padding = 1,
    -- Sets the maximum buffer name length.
    maximum_length = 30,
    -- Sets the minimum buffer name length.
    minimum_length = 2,
    -- If set, the letters for each buffer in buffer-pick mode will be
    -- assigned based on their name. Otherwise or in case all letters are
    -- already assigned, the behavior is to assign letters in order of
    -- usability (see order below)
    semantic_letters = true,
    -- Set the filetypes which barbar will offset itself for
    sidebar_filetypes = {
        -- Use the default values: {event = 'BufWinLeave', text = nil}
        NvimTree = true,
        --    -- Or, specify the text used for the offset:
        --    undotree = {text = 'undotree'},
        --    -- Or, specify the event which the sidebar executes when leaving:
        --    ['neo-tree'] = {event = 'BufWipeout'},
        --    -- Or, specify both
        --    Outline = {event = 'BufWinLeave', text = 'symbols-outline'},
    },
    -- New buffer letters are assigned in this order. This order is
    -- optimal for the qwerty keyboard layout but might need adjustment
    -- for other layouts.
    --  letters = 'asdfjkl;ghnmxcvbziowerutyqpASDFJKLGHNMXCVBZIOWERUTYQP',

    -- Sets the name of unnamed buffers. By default format is "[Buffer X]"
    -- where X is the buffer number. But only a static string is accepted here.
    --  no_name_title = nil,
})

-- barbar keymappings
-- clear TAB mapping before using it
vim.api.nvim_set_keymap("", "<TAB>", "<Nop>", { noremap = true, silent = true })

-- Move to previous/next
vim.keymap.set("n", "<TAB>", "<cmd>BufferNext<cr>", { noremap = true, silent = true })
vim.keymap.set("n", "<S-TAB>", "<cmd>BufferPrevious<cr>", { noremap = true, silent = true })

-- vim.keymap.set("n", "<A-,>", "<cmd>BufferPrevious<cr>", { noremap = true, silent = true })
-- vim.keymap.set("n", "<A-.>", "<cmd>BufferNext<cr>", { noremap = true, silent = true })

-- Re-order to previous/next
vim.keymap.set("n", "<A-<>", "<cmd>BufferMovePrevious<cr>", { noremap = true, silent = true })
vim.keymap.set("n", "<A->>", "<cmd>BufferMoveNext<cr>", { noremap = true, silent = true })

-- Goto buffer in position...
vim.keymap.set("n", "<A-1>", "<cmd>BufferGoto 1<cr>", { noremap = true, silent = true })
vim.keymap.set("n", "<A-2>", "<cmd>BufferGoto 2<cr>", { noremap = true, silent = true })
vim.keymap.set("n", "<A-3>", "<cmd>BufferGoto 3<cr>", { noremap = true, silent = true })
vim.keymap.set("n", "<A-4>", "<cmd>BufferGoto 4<cr>", { noremap = true, silent = true })
vim.keymap.set("n", "<A-5>", "<cmd>BufferGoto 5<cr>", { noremap = true, silent = true })
vim.keymap.set("n", "<A-6>", "<cmd>BufferGoto 6<cr>", { noremap = true, silent = true })
vim.keymap.set("n", "<A-7>", "<cmd>BufferGoto 7<cr>", { noremap = true, silent = true })
vim.keymap.set("n", "<A-8>", "<cmd>BufferGoto 8<cr>", { noremap = true, silent = true })
vim.keymap.set("n", "<A-9>", "<cmd>BufferGoto 9<cr>", { noremap = true, silent = true })
vim.keymap.set("n", "<A-0>", "<cmd>BufferLast<cr>", { noremap = true, silent = true })

-- Pin/unpin buffer
vim.keymap.set("n", "<A-p>", "<cmd>BufferPin<cr>", { noremap = true, silent = true })
-- Close buffer
-- vim.keymap.set("n", "<A-c>", "<cmd>BufferClose<cr>", { noremap = true, silent = true })
vim.keymap.set("n", "<C-x>", "<cmd>BufferClose<cr>", { noremap = true, silent = true, desc = "Close Buffer" })
-- Wipeout buffer
--                 :BufferWipeout
-- Close commands
--                 :BufferCloseAllButCurrent
--                 :BufferCloseAllButPinned
--                 :BufferCloseAllButCurrentOrPinned
--                 :BufferCloseBuffersLeft
--                 :BufferCloseBuffersRight
--
-- Magic buffer-picking mode
vim.keymap.set("n", "<C-p>", "<cmd>BufferPick<cr>", { noremap = true, silent = true })

-- Sort automatically by...
vim.keymap.set("n", "<space>bb", "<cmd>BufferOrderByBufferNumber<cr>", { noremap = true, silent = true })
vim.keymap.set("n", "<space>bd", "<cmd>BufferOrderByDirectory<cr>", { noremap = true, silent = true })
vim.keymap.set("n", "<space>bl", "<cmd>BufferOrderByLanguage<cr>", { noremap = true, silent = true })
vim.keymap.set("n", "<space>bw", "<cmd>BufferOrderByWindowNumber<cr>", { noremap = true, silent = true })

-- Other:
-- :BarbarEnable - enables barbar (enabled by default)
-- :BarbarDisable - very bad command, should never be used

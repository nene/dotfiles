local marks_ok, marks = pcall(require, "marks")

if not marks_ok then
    vim.notify("Plugin [marks] not loaded", vim.log.levels.WARN)
    return
end

local icons = require("base.icons")

marks.setup({
    refresh_interval = 250,
    mappings = {
        next = "<Right>m",   -- goto next mark
        prev = "<Left>m",    -- goto previous mark
        delete_line = "dl",  -- deletes all marks on cursor line
        delete_buf = "<leader>db", -- deletes all marks on cursor line
        -- set = "",  -- sets a mark
        -- delete = "",  -- delete a mark

        next_bookmark = "<Right>b", --goto next bookmark
        prev_bookmark = "<Left>b", --goto previous bookmark
        -- set_bookmark = "", -- set bookmark on cursor line
        delete_bookmark = "db", -- set bookmark on cursor line

        set_bookmark0 = "m0",
        set_bookmark1 = "m1",
        set_bookmark2 = "m2",
        set_bookmark3 = "m3",
        set_bookmark4 = "m4",
        set_bookmark5 = "m5",
        set_bookmark6 = "m6",
        set_bookmark7 = "m7",
        set_bookmark8 = "m8",
        set_bookmark9 = "m9",

        next_bookmark0 = "<Right>0",
        prev_bookmark0 = "<Left>0",

        next_bookmark1 = "<Right>1",
        prev_bookmark1 = "<Left>1",

        next_bookmark2 = "<Right>2",
        prev_bookmark2 = "<Left>2",

        next_bookmark3 = "<Right>3",
        prev_bookmark3 = "<Left>3",

        next_bookmark4 = "<Right>4",
        prev_bookmark4 = "<Left>4",

        next_bookmark5 = "<Right>5",
        prev_bookmark5 = "<Left>5",

        next_bookmark6 = "<Right>6",
        prev_bookmark6 = "<Left>6",

        next_bookmark7 = "<Right>7",
        prev_bookmark7 = "<Left>7",

        next_bookmark8 = "<Right>8",
        prev_bookmark8 = "<Left>8",

        next_bookmark9 = "<Right>9",
        prev_bookmark9 = "<Left>9",
    },

    bookmark_0 = {
        sign = icons.marks.Flag2,
        virt_text = " <-- bookmark0",
        annotate = false,
    },
    bookmark_1 = {
        sign = icons.marks.BookmarkFilled,
        virt_text = " <-- bookmark1",
        annotate = false,
    },
    bookmark_2 = {
        sign = icons.marks.BookmarkFilled,
        virt_text = " <-- bookmark2",
        annotate = false,
    },
    bookmark_3 = {
        sign = icons.marks.BookmarkFilled,
        virt_text = " <-- bookmark3",
        annotate = false,
    },
    bookmark_4 = {
        sign = icons.marks.BookmarkFilled,
        virt_text = " <-- bookmark4",
        annotate = false,
    },
    bookmark_5 = {
        sign = icons.marks.BookmarkFilled,
        virt_text = " <-- bookmark5",
        annotate = false,
    },
    bookmark_6 = {
        sign = icons.marks.BookmarkFilled,
        virt_text = " <-- bookmark6",
        annotate = false,
    },
    bookmark_7 = {
        sign = icons.marks.BookmarkFilled,
        virt_text = " <-- bookmark7",
        annotate = false,
    },
    bookmark_8 = {
        sign = icons.marks.BookmarkFilled,
        virt_text = " <-- bookmark8",
        annotate = false,
    },
    bookmark_9 = {
        sign = icons.marks.BookmarkFilled,
        virt_text = " <-- bookmark9",
        annotate = false,
    },
})

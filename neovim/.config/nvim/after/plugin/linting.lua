local lint_ok, lint = pcall(require, "lint")
if not lint_ok then
    vim.notify("Plugin [nvim-lint] not loaded", vim.log.levels.WARN)
    return
end

lint.linters_by_ft = {
    go = { "golangcilint" },
    git = { "gitlint" },
    sql = { "sqlfluff" },
    python = { "pylint" },
    kotlin = { "ktlint" },
    typescript = { "eslint_d" },
    javascript = { "eslint_d" },
}

local linter_augroup = vim.api.nvim_create_augroup("autolinter", { clear = true })

vim.api.nvim_create_autocmd({ "BufEnter", "BufWritePost", "InsertLeave" }, {
    group = linter_augroup,
    callback = function()
        lint.try_lint()
    end,
})

vim.keymap.set("n", "<leader>ll", function()
    lint.try_lint()
end, { noremap = true, silent = true, desc = "Trigger linting for current file" })

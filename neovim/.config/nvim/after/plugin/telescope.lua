local tele_ok, telescope = pcall(require, "telescope")

if not tele_ok then
    vim.notify("Plugin [telescope] not loaded", vim.log.levels.WARN)
    return
end

local icons = require("base.icons")

local builtin = require("telescope.builtin")
-- local utils = require("telescope.utils")
local actions = require("telescope.actions")
local fb_actions = telescope.extensions.file_browser.actions
local teleConfig = require("telescope.config")

-- clone the default Telescope configuration
local vimgrep_arguments = {
    unpack(teleConfig.values.vimgrep_arguments),
}
-- Allow to search hidden dotfiles
table.insert(vimgrep_arguments, "--hidden")
-- do not search the .git directory
table.insert(vimgrep_arguments, "--glob")
table.insert(vimgrep_arguments, "!**/.git/*")

telescope.setup({
    defaults = {
        prompt_prefix = icons.misc.Telescope2 .. " ",
        selection_caret = icons.arrows.ForwardUp .. " ",
        color_devicons = true,
        vimgrep_arguments = vimgrep_arguments,
        path_display = { "truncate" },
        initial_mode = "insert",
        selection_strategy = "reset",
        sorting_strategy = "ascending",
        layout_strategy = "horizontal",
        set_env = { ["COLORTERM"] = "truecolor" },
        layout_config = {
            horizontal = { prompt_position = "top" },
            vertical = { prompt_position = "top" },
            bottom_pane = { prompt_position = "top" },
            center = { prompt_position = "top" },
        },
        borderchars = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
        file_ignore_patterns = {
            "node_modules/*",
            "build",
            "dist",
            "yarn.lock",
            ".mypy_cache/*",
            "env/*",
            "vendor/*",
            "*/__pycache__",
        },
        highlight = { enable = true },
        indent = { enable = true },
        incremental_selection = {
            enable = true,
            keymaps = {
                init_selection = "<c-space>",
                node_incremental = "<c-space>",
                scope_incremental = "<c-s>",
                node_decremental = "<M-space>",
            },
        },
        mappings = {
            i = {
                ["<esc>"] = actions.close,
                ["<C-c>"] = actions.close,
                ["<C-j>"] = actions.move_selection_next,
                ["<C-k>"] = actions.move_selection_previous,
                ["<Down>"] = actions.move_selection_next,
                ["<Up>"] = actions.move_selection_previous,

                ["gg"] = actions.move_to_top,
                ["G"] = actions.move_to_bottom,

                ["<C-u>"] = actions.preview_scrolling_up,
                ["<C-d>"] = actions.preview_scrolling_down,
                ["<PageUp>"] = actions.preview_scrolling_up,
                ["<PageDown>"] = actions.preview_scrolling_down,

                ["<CR>"] = actions.select_default + actions.center,
                ["<C-h>"] = actions.select_horizontal,
                ["<C-v>"] = actions.select_vertical,
                ["<C-t>"] = actions.select_tab,
                ["<C-q>"] = actions.smart_send_to_qflist + actions.open_qflist,

                -- ["<Tab>"] = actions.toggle_selection + actions.move_selection_worse,
                -- ["<S-Tab>"] = actions.toggle_selection + actions.move_selection_better,
            },
            n = {
                ["<esc>"] = actions.close,
                ["<C-c>"] = actions.close,
                ["<C-j>"] = actions.move_selection_next,
                ["<C-k>"] = actions.move_selection_previous,
                ["<Down>"] = actions.move_selection_next,
                ["<Up>"] = actions.move_selection_previous,

                ["gg"] = actions.move_to_top,
                ["G"] = actions.move_to_bottom,

                ["<C-u>"] = actions.preview_scrolling_up,
                ["<C-d>"] = actions.preview_scrolling_down,
                ["<PageUp>"] = actions.preview_scrolling_up,
                ["<PageDown>"] = actions.preview_scrolling_down,

                ["<CR>"] = actions.select_default + actions.center,
                ["<C-h>"] = actions.select_horizontal,
                ["<C-v>"] = actions.select_vertical,
                ["<C-t>"] = actions.select_tab,
                ["<C-q>"] = actions.smart_send_to_qflist + actions.open_qflist,

                -- ["<Tab>"] = actions.toggle_selection + actions.move_selection_worse,
                -- ["<S-Tab>"] = actions.toggle_selection + actions.move_selection_better,
            },
        },
    },
    pickers = {
        find_files = {
            -- hidden  = true will still show the inside of ".git/" as its not gitignored
            find_command = { "rg", "--files", "--hidden", "--glob", "!**/.git/*" },
            follow = true,
            hidden = true,
            no_ignore = false,
            no_ignore_parent = false,
        },
    },
    buffers = {
        theme = "dropdown",
        sort_mru = true,
        show_all_buffers = true,
    },
    extensions = {
        fzf = {
            fuzzy = true,
            override_generic_sorter = true,
            override_file_sorter = true,
            case_mode = "smart_case",
        },
        ["ui-select"] = {
            themes = "dropdown",
        },
        frecency = {
            show_unindexed = true,
        },
        file_browser = {
            -- disables netrw and use telescope-file-browser in its place
            hijack_netrw = true, -- use telescope file browser when opening directory paths
            theme = "ivy",
            display_stat = false, -- don't show file stat
            grouped = true, -- group initial sorting by directories and then files
            hidden = true,  -- show hidden files
            hide_parent_dir = false, -- hide `../` in the file browser
            prompt_path = false, -- show the current relative path from cwd as the prompt prefix
            use_fd = true,  -- use `fd` instead of plenary, make sure to install `fd`
            mappings = {
                ["i"] = {
                    ["<C-h>"] = fb_actions.goto_home_dir,
                    ["<C-p>"] = fb_actions.goto_parent_dir,
                    ["<C-o>"] = fb_actions.open,
                    ["<C-a>"] = fb_actions.create,
                    ["<C-d>"] = fb_actions.remove,
                    ["<C-r>"] = fb_actions.rename,
                },
                ["n"] = {
                    ["<C-h>"] = fb_actions.goto_home_dir,
                    ["<C-p>"] = fb_actions.goto_parent_dir,
                    ["<C-o>"] = fb_actions.open,
                    ["<C-a>"] = fb_actions.create,
                    ["<C-d>"] = fb_actions.remove,
                    ["<C-r>"] = fb_actions.rename,
                },
            },
        },
        textobjects = {
            select = {
                enable = true,
                lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
                keymaps = {
                    -- You can use the capture groups defined in textobjects.scm
                    ["aa"] = "@parameter.outer",
                    ["ia"] = "@parameter.inner",
                    ["af"] = "@function.outer",
                    ["if"] = "@function.inner",
                    ["ac"] = "@class.outer",
                    ["ic"] = "@class.inner",
                },
            },
            move = {
                enable = true,
                set_jumps = true, -- whether to set jumps in the jumplist
                goto_next_start = {
                    ["]m"] = "@function.outer",
                    ["]]"] = "@class.outer",
                },
                goto_next_end = {
                    ["]M"] = "@function.outer",
                    ["]["] = "@class.outer",
                },
                goto_previous_start = {
                    ["[m"] = "@function.outer",
                    ["[["] = "@class.outer",
                },
                goto_previous_end = {
                    ["[M"] = "@function.outer",
                    ["[]"] = "@class.outer",
                },
            },
            swap = {
                enable = true,
                swap_next = {
                    ["<leader>a"] = "@parameter.inner",
                },
                swap_previous = {
                    ["<leader>A"] = "@parameter.inner",
                },
            },
        },
    },
})

-- Load extentions
telescope.load_extension("fzf")
telescope.load_extension("file_browser")
telescope.load_extension("ui-select")
telescope.load_extension("spell_errors")

-- File Explorer
vim.keymap.set(
    "n",
    "<leader>fe",
    "<cmd>Telescope file_browser<cr>",
    { noremap = true, silent = true, desc = "File Explorer" }
)

-- File Path Explorer
vim.keymap.set(
    "n",
    "<leader>fp",
    "<cmd>Telescope file_browser path=%:p:h select_buffer=true<cr>",
    { noremap = true, silent = true, desc = "File Path Browser" }
)

vim.keymap.set("n", "<C-p>", builtin.git_files, { noremap = true, silent = true, desc = "Search GIT files" })
vim.keymap.set("n", "<leader>ff", builtin.find_files, { noremap = true, silent = true, desc = "Find Files" })

vim.keymap.set("n", "<leader>fg", function()
    builtin.live_grep()
end, { noremap = true, silent = true, desc = "Find Word" })

-- vim.keymap.set("n", "<leader>fg", function()
--     builtin.live_grep({ grep_open_files = true, prompt_title = "Live grep in open files" })
-- end, { noremap = true, silent = true, desc = "Find Grep" })

vim.keymap.set(
    "n",
    "<leader>fw",
    builtin.grep_string,
    { noremap = true, silent = true, desc = "Find Word under cursor" }
)

vim.keymap.set("n", "<leader>fs", function()
    local msg_prompt = " Search For " .. icons.misc.MagGlassRight .. " : "
    local input_string = vim.fn.input(msg_prompt)
    if input_string == "" then
        return
    end
    builtin.grep_string({ search = input_string })
end, { noremap = true, silent = true, desc = "Find String Globally" })

vim.keymap.set("n", "<leader>fh", builtin.help_tags, { noremap = true, silent = true, desc = "Find Help Tags" })
vim.keymap.set("n", "<leader>fb", builtin.buffers, { noremap = true, silent = true, desc = "Find buffers" })
vim.keymap.set("n", "<leader>fd", builtin.diagnostics, { noremap = true, silent = true, desc = "Find Diagnostics" })

-- Document Symbols
vim.keymap.set(
    "n",
    "<leader>ds",
    builtin.treesitter,
    { noremap = true, silent = true, desc = "List Treesitter Symbols" }
)

-- Spell Suggest
vim.keymap.set(
    "n",
    "<leader>ss",
    builtin.spell_suggest,
    { noremap = true, silent = true, desc = "List Spell Suggestions" }
)

-- Old Files (recently open)
vim.keymap.set(
    "n",
    "<leader>fo",
    builtin.oldfiles,
    { noremap = true, silent = true, desc = " Find recently opened files" }
)

-- Marks
vim.keymap.set("n", "<leader>fm", builtin.marks, { noremap = true, silent = true, desc = " Find marks" })

-- You can pass additional configuration to telescope to change theme, layout, etc.
vim.keymap.set("n", "<leader>/", function()
    builtin.current_buffer_fuzzy_find(require("telescope.themes").get_dropdown({
        winblend = 10,
        previewer = false,
        layout_config = { width = 0.7 },
    }))
end, { noremap = true, silent = true, desc = "[/] Fuzzily search in current buffer]" })

-- Spelling
vim.keymap.set("n", "<leader>sp", function()
    require("telescope").extensions.spell_errors.spell_errors()
end, { noremap = true, silent = true, desc = " Spell Errors" })

-- Helpful Default Mappings
-- https://github.com/nvim-telescope/telescope.nvim/blob/master/lua/telescope/mappings.lua
-- Default Mapping for Splits
-- <C-x> go to file selection as a split
-- <C-v> go to file selection as a vsplit
-- <C-t> go to file selection in a new tab

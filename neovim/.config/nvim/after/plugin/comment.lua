local comment_ok, comment = pcall(require, "Comment")

if not comment_ok then
    vim.notify("Plugin [commment] not loaded", vim.log.levels.WARN)
    return
end

comment.setup()

local comment_api = require("Comment.api")
vim.keymap.set("n", "<C-_>", comment_api.toggle.linewise.current, { noremap = true, silent = true })
-- vim.keymap.set("n", "<C-c>", comment_api.toggle.linewise.current, { noremap = true, silent = true })

-- vim.keymap.set("n", "<C-_>", comment.api.toggle.linewise.current, { noremap = true, silent = true })
-- vim.keymap.set("n", "<C-c>", comment.api.toggle.linewise.current, { noremap = true, silent = true })

vim.keymap.set(
    "v",
    "<C-_>",
    "<esc><cmd>lua require('Comment.api').toggle.linewise(vim.fn.visualmode())<cr>",
    { noremap = true, silent = true }
)
-- vim.keymap.set(
--     "v",
--     "<C-c>",
--     "<esc><cmd>lua require('Comment.api').toggle.linewise(vim.fn.visualmode())<cr>",
--     { noremap = true, silent = true }
-- )

local hop_ok, hop = pcall(require, "hop")
if not hop_ok then
    -- vim.notify("Plugin [hop] not loaded", vim.log.levels.WARN)
    return
end

hop.setup()

local directions = require("hop.hint").HintDirection

vim.keymap.set({ "n", "x", "o" }, "s", "<cmd>HopWordMW<cr>", { noremap = true, silent = true, desc = "Hop Word" })
vim.keymap.set({ "n", "x", "o" }, "S", "<cmd>HopNodes<cr>", { noremap = true, silent = true, desc = "Hop Nodes" })

-- place this in one of your configuration file(s)
vim.keymap.set("", "f", function()
    hop.hint_char1({ direction = directions.AFTER_CURSOR, current_line_only = true })
end, { remap = true })

vim.keymap.set("", "F", function()
    hop.hint_char1({ direction = directions.BEFORE_CURSOR, current_line_only = true })
end, { remap = true })

vim.keymap.set("", "t", function()
    hop.hint_char1({ direction = directions.AFTER_CURSOR, current_line_only = true, hint_offset = -1 })
end, { remap = true })

vim.keymap.set("", "T", function()
    hop.hint_char1({ direction = directions.BEFORE_CURSOR, current_line_only = true, hint_offset = 1 })
end, { remap = true })

-- vim.api.nvim_create_autocmd("HopColorScheme", {
-- callback = function()
-- vim.api.nvim_set_hl(0, "HopNextKey", { fg = "#ff9900", bold = true, ctermfg = 198 })
-- vim.api.nvim_set_hl(0, "HopNextKey1", { fg = "#ff9900", bold = true, ctermfg = 198 })
-- vim.api.nvim_set_hl(0, "HopNextKey2", { fg = "#ff9900", bold = true, ctermfg = 198 })
---- vim.api.nvim_set_hl(0, "HopNextKey", { fg = "#ff9900", bold = true, ctermfg = 198, cterm = { bold = true } })
---- vim.api.nvim_set_hl(0, "HopNextKey1", { fg = "#ff9900", bold = true, ctermfg = 198, cterm = { bold = true } })
---- vim.api.nvim_set_hl(0, "HopNextKey2", { fg = "#ff9900", bold = true, ctermfg = 198, cterm = { bold = true } })
-- end,
-- })

local conform_ok, conform = pcall(require, "conform")
if not conform_ok then
    vim.notify("Plugin [conform] not loaded", vim.log.levels.WARN)
    return
end

-- Conform will run multiple formatters sequentially
-- Use a sub-list to run only the first available formatter
conform.setup({
    formatters_by_ft = {
        go = { { "gofumpt", "goimports" } },
        sql = { "sql_formatter" },
        lua = { "stylua" },
        rust = { "rustfmt" },
        -- proto = { "buf" },
        json = { "prettier" },
        yaml = { "prettier", "yamlfix" },
        java = { "google-java-format" },
        python = { "isort", "black" },
        markdown = { "prettier" },
        javascript = { "prettier", "jq" },
    },
    format_on_save = {
        -- these options will be passes to conform.format()
        lsp_fallback = true, -- will call vim.lsp.buf.format()
        async = false,
        timeout_ms = 500,
    },
})

vim.keymap.set({ "n", "v" }, "<leader>l", function()
    conform.format({
        lsp_fallback = true,
        async = false,
        timeout_ms = 1000,
    })
end, { noremap = true, silent = true, desc = "Format file or range (in visual mode)" })

-- local formatter_augroup = vim.api.nvim_create_augroup("autoformatter", { clear = true })
-- vim.api.nvim_create_autocmd({ "BufWritePre" }, {
--     group = formatter_augroup,
--     callback = function()
--         require("conform").format({ bufnr = args.buf })
--     end,
-- })

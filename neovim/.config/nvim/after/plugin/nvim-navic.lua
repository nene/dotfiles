local navic_ok, navic = pcall(require, "nvim-navic")
if not navic_ok then
    vim.opt.winbar = "%f"
    vim.notify("Plugin [nvim-navic] not loaded", vim.log.levels.WARN)
    return
end

-- use common icons to be consistent throughout
local icons = require("base.icons")

navic.setup({
    icons = {
        File = icons.lang_icons.File,
        Module = icons.lang_icons.Module,
        Namespace = icons.lang_icons.Namespace,
        Package = icons.lang_icons.Package,
        Class = icons.lang_icons.Class,
        Method = icons.lang_icons.Method,
        Property = icons.lang_icons.Property,
        Field = icons.lang_icons.Field,
        Constructor = icons.lang_icons.Constructor,
        Enum = icons.lang_icons.Enum,
        Interface = icons.kind_icons.Interface,
        Function = icons.lang_icons.Function,
        Variable = icons.lang_icons.Variable,
        Constant = icons.lang_icons.Constant,
        String = icons.lang_icons.String,
        Number = icons.lang_icons.Number,
        Boolean = icons.lang_icons.Boolean,
        Array = icons.lang_icons.Array,
        Object = icons.lang_icons.Object,
        Key = icons.lang_icons.Key,
        Null = icons.lang_icons.Null,
        EnumMember = icons.lang_icons.EnumMember,
        Struct = icons.lang_icons.Struct,
        Event = icons.lang_icons.Event,
        Operator = icons.lang_icons.Operator,
        TypeParameter = icons.lang_icons.TypeParameter,
    },
    lsp = {
        auto_attach = false,
        preference = nil,
    },
    highlight = true,
    separator = " " .. icons.arrows.ChevronRight .. " ",
    depth_limit = 0,
    depth_limit_indicator = "..",
    safe_output = true,
    lazy_update_context = false,
    click = true,
    format_text = function(text)
        return text
    end,
})

-- vim.o.statusline = "%{%v:lua.require'nvim-navic'.get_location()%}"
--  OR
vim.o.winbar = "%{%v:lua.require'nvim-navic'.get_location()%}"

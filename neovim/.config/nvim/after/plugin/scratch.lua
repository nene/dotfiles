local scratch_ok, scratch = pcall(require, "scratch")

if not scratch_ok then
    vim.notify("Plugin [scratch] not loaded", vim.log.levels.WARN)
    print("scratch?", vim.inspect(scratch))
    return
end

vim.keymap.set("n", "<leader>bs", function()
    require("scratch").open()
end, { noremap = true, silent = true, desc = " Open scratch buffer" })

vim.keymap.set("n", "<leader>bS", function()
    require("scratch").split()
end, { noremap = true, silent = true, desc = " Open scratch buffer (split)" })

-- NOTE: Configurations for LSP and CMP (completion)

local icons = require("base.icons")

local sign_icons = {
    {
        name = "DiagnosticSignError",
        icon = icons.diagnostic_big.Error,
    },
    {
        name = "DiagnosticSignWarn",
        icon = icons.diagnostic_big.Warning,
    },
    {
        name = "DiagnosticSignHint",
        icon = icons.diagnostic_big.BoldHint,
    },
    {
        name = "DiagnosticSignInfo",
        icon = icons.diagnostic_big.Information,
    },
}

-- assign custom icons for diagtnostics
for _, sign in ipairs(sign_icons) do
    vim.fn.sign_define(sign.name, { texthl = sign.name, text = sign.icon, numhl = "" })
end

local lsp_zero = require("lsp-zero")

lsp_zero.preset("recommended")

--lsp_zero.set_sign_icons({
--  error = '✘',
--  warn = '▲',
--  hint = '⚑',
--  info = '»'
--})

-- on_attach() happens on every single buffer
-- that has an LSP associated with it.
lsp_zero.on_attach(function(client, bufnr)
    -- lsp_zero.default_keymaps({buffer = bufnr})

    local opts = { buffer = bufnr, remap = false }

    -- signature help
    vim.keymap.set("n", "K", function()
        vim.lsp.buf.hover()
    end, opts)

    vim.keymap.set("i", "<C-k>", function()
        vim.lsp.buf.signature_help()
    end, opts)

    -- jump to function definition/declaration/implementation
    vim.keymap.set("n", "gd", function()
        vim.lsp.buf.definition()
    end, opts)

    vim.keymap.set("n", "gD", function()
        vim.lsp.buf.declaration()
    end, opts)

    vim.keymap.set("n", "gI", function()
        vim.lsp.buf.implementation()
    end, opts)

    -- candidate to remove
    vim.keymap.set("n", "gt", function()
        vim.lsp.buf.type_definition()
    end, opts)

    -- various
    -- candidate to remove
    vim.keymap.set("n", "<leader>vws", function()
        vim.lsp.buf.workspace_symbol()
    end, opts)

    vim.keymap.set("n", "<leader>ca", function()
        vim.lsp.buf.code_action()
    end, opts)

    -- candidate to remove
    vim.keymap.set("n", "<leader>rr", function()
        vim.lsp.buf.references()
    end, opts)

    vim.keymap.set("n", "<leader>rn", function()
        vim.lsp.buf.rename()
    end, opts)

    -- supress virtual test for diagnostics
    -- vim.diagnostic.config({
    --     virtual_text = {
    --         format = function()
    --             return ""
    --         end,
    --     },
    --     signs = true,
    -- })

    -- diagnostic keymaps
    vim.keymap.set("n", "[d", function()
        vim.diagnostic.goto_next()
    end, opts)

    vim.keymap.set("n", "]d", function()
        vim.diagnostic.goto_prev()
    end, opts)

    vim.keymap.set("n", "<leader>vd", function()
        vim.diagnostic.open_float()
    end, opts)

    vim.keymap.set("n", "<leader>df", vim.diagnostic.open_float, opts)
    vim.keymap.set("n", "<leader>dl", vim.diagnostic.setloclist, opts)

    vim.keymap.set("n", "<leader>di", function()
        vim.lsp.diagnostic.disable()
    end, opts)
    vim.keymap.set("n", "<leader>de", function()
        vim.lsp.diagnostic.enable()
    end, opts)

    -- formatting document
    vim.keymap.set("n", "<leader>f", function()
        vim.lsp.buf.format({ async = false, timeout_ms = 10000 })
    end, opts)

    -- ======= Telescope Support for LSP ======
    local builtin = require("telescope.builtin")

    vim.keymap.set(
        "n",
        "<leader>gr",
        require("telescope.builtin").lsp_references,
        { noremap = true, silent = true, desc = "[G]oto [R]eferences" }
    )

    vim.keymap.set(
        "n",
        "gi",
        require("telescope.builtin").lsp_implementations,
        { noremap = true, silent = true, desc = "[G]oto [I]mplementation" }
    )

    vim.keymap.set(
        "n",
        "<leader>ds",
        builtin.lsp_document_symbols,
        { noremap = true, silent = true, desc = "[D]ocument [S]ymbols" }
    )

    vim.keymap.set(
        "n",
        "<leader>ws",
        builtin.lsp_dynamic_workspace_symbols,
        { noremap = true, silent = true, desc = "[W]orkspace [S]ymbols" }
    )

    -- highlight the symbol under the cursor.
    -- lsp_zero.highlight_symbol(client, bufnr)

    vim.keymap.set("n", "<leader>td", function()
        local diag_status = 1 -- 1 is show; 0 is hide
        if diag_status == 1 then
            diag_status = 0
            vim.diagnostic.hide()
        else
            diag_status = 1
            vim.diagnostic.show()
        end
    end)

    -- nvim-navic. code path based on LSP
    if client.server_capabilities.documentSymbolProvider then
        local navic = require("nvim-navic")
        navic.highlight = true
        navic.attach(client, bufnr)
    end
end)

lsp_zero.format_on_save({
    format_opts = {
        async = false,
        timeout_ms = 10000,
    },
    servers = {
        ["lua_ls"] = { "lua" },
        ["gopls"] = { "go" },
        ["pyright"] = { "python" },
        ["rust_analyzer"] = { "rust" },
        ["jdtls"] = { "java" },
    },
})

-- require("lspconfig").gopls.setup { on_attach = require("lsp-format").on_attach }

require("mason").setup({})
require("mason-lspconfig").setup({
    -- available LSP servers https://github.com/williamboman/mason-lspconfig.nvim#available-lsp-servers
    ensure_installed = {
        "bashls",
        "gopls",
        "jsonls",
        "lua_ls",
        "gradle_ls",
        "jdtls",
        "pyright",
        "rust_analyzer",
        "java_language_server",
        "kotlin_language_server",
    },
    handlers = {
        lsp_zero.default_setup,
        lua_ls = function()
            local lua_opts = lsp_zero.nvim_lua_ls()
            require("lspconfig").lua_ls.setup(lua_opts)
        end,
        gopls = function()
            require("lspconfig").gopls.setup({
                settings = {
                    gopls = {
                        completeUnimported = true,
                        usePlaceholders = true,
                    },
                    analyses = {
                        unusedparams = true,
                    },
                },
            })
        end,
    },
})

local mason_tool_installer = require("mason-tool-installer")
mason_tool_installer.setup({
    ensure_installed = {
        "prettier",     -- prettier formatter
        "stylua",       -- lua formatter
        "isort",        -- python formatter
        "black",        -- python formatter
        "pylint",       -- python linter
        "gitlint",      -- git linter
        "eslint_d",     -- js linter
        "jsonlint",     -- json linter
        "golangci-lint", -- golang linter
        "gofumpt",      -- golang formatter
        "goimports",    -- golang formatter
        "gotests",      -- golang testing
        "google-java-format", -- java formatter
    },
})

vim.diagnostic.config({
    float = {
        focusable = false,
        style = "minimal",
        border = "rounded",
        source = "always",
        header = "",
        prefix = "",
    },
})

vim.keymap.set("n", "<leader>ma", "<cmd>Mason<cr>", { noremap = true, silent = true })

local headlines_ok, headlines = pcall(require, "headlines")

if not headlines_ok then
    vim.notify("Plugin [headlines] not loaded", vim.log.levels.WARN)
    return
end

-- local icons = require("base.icons")

headlines.setup({})

-- vim.cmd([[highlight Headline1 guibg=#1e2718]])
-- vim.cmd([[highlight Headline2 guibg=#21262d]])
-- vim.cmd([[highlight CodeBlock guibg=#1c1c1c]])
-- vim.cmd([[highlight Dash guibg=#D19A66 gui=bold]])

-- headlines.setup({
--     markdown = {
--         headline_highlights = { "Headline1", "Headline2" },
--     },
--     org = {
--         headline_highlights = { "Headline1", "Headline2" },
--     },
--     neoorg = {
--         headline_highlights = { "Headline1", "Headline2" },
--     },
-- })

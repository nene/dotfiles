local nvim_dap_ok, nvim_dap = pcall(require, "mason-nvim-dap")
if not nvim_dap_ok then
    vim.notify("Plugin [mason-nvim-dap] not loaded", vim.log.levels.WARN)
    return
end

nvim_dap.setup({
    ensure_installed = {
        "bash",
        "python",
        "delve",
    },
    automatic_installation = true,
})

local dap_ok, dap = pcall(require, "dap")
if not dap_ok then
    vim.notify("Plugin [dap] failed to load", vim.log.levels.WARN)
    return
end

--- Keymaps ---
vim.keymap.set("n", "<F2>", function()
    dap.step_into()
end, { noremap = true, silent = true, desc = "Step Into" })

vim.keymap.set("n", "<F3>", function()
    dap.step_over()
end, { noremap = true, silent = true, desc = "Step Over" })

vim.keymap.set("n", "<F4>", function()
    dap.step_out()
end, { noremap = true, silent = true, desc = "Step Out" })

vim.keymap.set("n", "<F5>", function()
    dap.continue()
end, { noremap = true, silent = true, desc = "Continue" })

--vim.keymap.set("n", "<leader>co", function() dap.continue() end, { noremap = true, silent = true, desc = "Continue" })
--vim.keymap.set("n", "<leader>so", function() dap.step_over() end, { noremap = true, silent = true, desc = "Step Over" })
--vim.keymap.set("n", "<leader>si", function() dap.step_into() end, { noremap = true, silent = true, desc = "Step Into" })
--vim.keymap.set("n", "<leader>sq", function() dap.step_out() end, { noremap = true, silent = true, desc = "Step Out" })

vim.keymap.set("n", "<leader>dk", function()
    dap.up()
end, { noremap = true, silent = true, desc = "Up" })

vim.keymap.set("n", "<leader>dj", function()
    dap.down()
end, { noremap = true, silent = true, desc = "Down" })

vim.keymap.set("n", "<leader>dl", function()
    dap.run_last()
end, { noremap = true, silent = true, desc = "Run to last" })

vim.keymap.set("n", "<leader>dc", function()
    dap.run_to_cursor()
end, { noremap = true, silent = true, desc = "Run to cursor" })

vim.keymap.set("n", "<leader>b", function()
    dap.toggle_breakpoint()
end, { noremap = true, silent = true, desc = "Toggle Breakpoint" })

vim.keymap.set("n", "<leader>B", function()
    dap.set_breakpoint(vim.fn.input("Breakpoint condition: "))
end, { noremap = true, silent = true, desc = "Breaking Condition" })

vim.keymap.set("n", "<leader>lp", function()
    dap.set_breakpoint(nil, nil, vim.fn.input("Log point message: "))
end, { noremap = true, silent = true, desc = "" })

vim.keymap.set("n", "<leader>dr", function()
    dap.repl.open()
end, { noremap = true, silent = true, desc = "" })

vim.keymap.set(
    "n",
    "<leader>td",
    vim.lsp.buf.type_definition,
    { noremap = true, silent = true, desc = "Type Definition" }
)

-- vim.keymap.set("n", "<leader>dt", ":lua require('dap-go').debug_test()<CR>", { desc = "debug Test" })

local icons = require("base.icons")
vim.fn.sign_define("DapBreakpoint", {
    text = icons.debug_ui.Breakpoint,
    texthl = "DapBreakpoint",
    linehl = "DapBreakpoint",
    numhl = "DapBreakpoint",
})

-- DAP GUI
local dapui_ok, dapui = pcall(require, "dapui")
if not dapui_ok then
    vim.notify("Plugin [dapui] failed to load", vim.log.levels.WARN)
end

dapui.setup()

vim.keymap.set("n", "<leader>dt", function()
    dapui.toggle()
end, { noremap = true, silent = true })

vim.keymap.set("n", "<leader>do", function()
    dapui.open()
end, { noremap = true, silent = true })

vim.keymap.set("n", "<leader>dc", function()
    dapui.close()
end, { noremap = true, silent = true })

vim.keymap.set("n", "<leader>dr", function()
    dapui.open({ reset = true })
end, { noremap = true, silent = true })

require("nvim-dap-virtual-text").setup()
require("dap-go").setup()

dap.listeners.after.event_initialized["dapui_config"] = function()
    dapui.open({})
end

dap.listeners.before.event_terminated["dapui_config"] = function()
    dapui.close({})
end

dap.listeners.before.event_exited["dapui_config"] = function()
    dapui.close({})
end

local tele_ok, tele = pcall(require, "telescope")
if not tele_ok then
    return
end

-- Telescope Git mappings
tele.load_extension("dap")

-- require'telescope'.extensions.dap.commands{}
-- require'telescope'.extensions.dap.configurations{}
-- require'telescope'.extensions.dap.list_breakpoints{}
-- require'telescope'.extensions.dap.variables{}
-- require'telescope'.extensions.dap.frames{}
--
-- :Telescope dap commands
-- :Telescope dap configurations
-- :Telescope dap list_breakpoints
-- :Telescope dap variables
-- :Telescope dap frames

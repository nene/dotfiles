local aerial_ok, aerial = pcall(require, "aerial")

if not aerial_ok then
    -- vim.notify("Plugin [aerial] not loaded", vim.log.levels.WARN)
    return
end

local icons = require("base.icons")

aerial.setup({
    layout = {
        min_width = 40,
    },
    -- optionally use on_attach to set keymaps when aerial has attached to a buffer
    on_attach = function(bufnr)
        -- Jump forwards/backwards with '{' and '}'
        -- vim.keymap.set("n", "<", "<cmd>AerialPrev<cr>", { noremap = true, silent = true, buffer = bufnr })
        -- vim.keymap.set("n", "<", "<cmd>AerialPrev<cr>", { noremap = true, silent = true, buffer = bufnr })
        -- vim.keymap.set("n", "<TAB>", "<cmd>AerialNext<cr>", { noremap = true, silent = true, buffer = bufnr })
        -- vim.keymap.set("n", "<S-TAB>", "<cmd>AerialPrev<cr>", { noremap = true, silent = true, buffer = bufnr })
        vim.keymap.set("n", "<S-Down>", "<cmd>AerialNext<cr>", { noremap = true, silent = true, buffer = bufnr })
        vim.keymap.set("n", "<S-Up>", "<cmd>AerialPrev<cr>", { noremap = true, silent = true, buffer = bufnr })
    end,
})

-- You probably also want to set a keymap to toggle aerial
vim.keymap.set(
    "n",
    "<leader>a",
    "<cmd>AerialToggle!<cr>",
    { noremap = true, silent = true, desc = "Toggle symbols outline" }
)

-- Enable Telescope extension
local tele_ok, telescope = pcall(require, "telescope")

if not tele_ok then
    vim.notify("Plugin [telescope] failed to load", vim.log.levels.WARN)
    return
end

telescope.load_extension("aerial")
telescope.setup({
    extensions = {
        aerial = {
            -- Display symbols as <root>.<parent>.<symbol>
            show_nesting = {
                ["_"] = false, -- This key will be the default
                json = true, -- You can set the option for specific filetypes
                yaml = true,
            },
        },
    },
})

vim.keymap.set(
    "n",
    "<leader>ts",
    "<cmd>Telescope aerial<cr>",
    { noremap = true, silent = true, desc = "Document Symbols" }
)

-- --------------------------------------------------------------------------

local ok, symbols_outline = pcall(require, "symbols-outline")

if not ok then
    -- vim.notify("Plugin [symbols-outline] failed to load", vim.log.levels.WARN)
    return
end

vim.keymap.set(
    "n",
    "<leader>s",
    "<cmd>SymbolsOutline<cr>",
    { noremap = true, silent = true, desc = "Toggle symbols outline" }
)

symbols_outline.setup({
    keymaps = { -- These keymaps can be a string or a table for multiple keys
        close = { "<Esc>", "q" },
        goto_location = "<Cr>",
        focus_location = "o",
        hover_symbol = "<C-space>",
        toggle_preview = "K",
        rename_symbol = "r",
        code_actions = "a",
        fold = "h",
        unfold = "l",
        fold_all = "W",
        unfold_all = "E",
        fold_reset = "R",
    },
    lsp_blacklist = {},
    symbol_blacklist = {},
    symbols = {
        Array = { icon = icons.small_icons.Array, hl = "@constant" },
        Boolean = { icon = icons.lang_icons.Boolean, hl = "@boolean" },
        Constructor = { icon = icons.small_icons.Constructor, hl = "@constructor" },
        Component = { icon = icons.small_icons.Component, hl = "@function" },
        Constant = { icon = icons.small_icons.Constant, hl = "@constant" },
        Class = { icon = icons.lang_icons.Class, hl = "@type" },
        Enum = { icon = icons.small_icons.Enum, hl = "@type" },
        EnumMember = { icon = icons.small_icons.Enum, hl = "@field" },
        Event = { icon = icons.kind_icons.EventBold, hl = "@type" },
        Field = { icon = icons.small_icons.Field, hl = "@field" },
        File = { icon = icons.ui_small.FileIcon, hl = "@text.uri" },
        Fragment = { icon = icons.small_icons.Fragment, hl = "@constant" },
        Function = { icon = icons.lang_icons.Function, hl = "@function" },
        Interface = { icon = icons.kind_icons.Interface, hl = "@type" },
        Key = { icon = icons.lang_icons.Key, hl = "@type" },
        Namespace = { icon = icons.lang_icons.Namespace, hl = "@namespace" },
        Number = { icon = icons.small_icons.Number, hl = "@number" },
        Null = { icon = icons.small_icons.Null, hl = "@type" },
        Method = { icon = icons.small_icons.Method, hl = "@method" },
        Module = { icon = icons.small_icons.Module2, hl = "@namespace" },
        Object = { icon = icons.lang_icons.Object, hl = "@type" },
        Operator = { icon = icons.lang_icons.Operator, hl = "@operator" },
        Package = { icon = icons.small_icons.Package, hl = "@namespace" },
        Property = { icon = icons.small_icons.Property, hl = "@method" },
        String = { icon = icons.small_icons.String, hl = "@string" },
        Struct = { icon = icons.lang_icons.Struct, hl = "@type" },
        TypeParameter = { icons.lang_icons.TypeParameter, hl = "@parameter" },
        Variable = { icon = icons.small_icons.Variable, hl = "@constant" },
    },
})

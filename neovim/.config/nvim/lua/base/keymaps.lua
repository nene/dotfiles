vim.api.nvim_set_keymap("", "<Space>", "<Nop>", { noremap = true, silent = true })
vim.g.mapleader = " "
vim.g.maplocalleader = " "

--- Shorten function name
--- local keymap = vim.api.nvim_set_keymap

--- Modes
---   normal_mode = "n",
---   insert_mode = "i",
---   visual_mode = "v",
---   visual_block_mode = "x",
---   term_mode = "t",
---   command_mode = "c",

-- GOAT remap - quick access to command
vim.keymap.set("n", ";", ":")
--vim.api.nvim_set_keymap("n", ";", ":", { noremap = true, silent = true })

-- Netrw
-- vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)

-- clear search highlights
vim.keymap.set("n", "<leader>nh", "<cmd>nohlsearch<cr>", { silent = true })

-- UNDO mappings
vim.keymap.set({ "n", "v", "i", "x" }, "<C-z>", "<C-r>", { noremap = true, silent = true })

-- get out of insert mode
vim.keymap.set("i", "<C-c>", "<Esc>")

-- another way to save the file
vim.keymap.set( { "n", "i", "v", "x", "s" }, "<C-s>", "<cmd>w<cr><esc>",
    { noremap = true, silent = false, desc = "Write/Save file" }
)

-- Select all text
vim.keymap.set("n", "<C-a>", "ggVG", { noremap = true, silent = true, desc = "Select all" })

-- Save without formatting
vim.keymap.set( "n", "<leader>wf", "<cmd>noautocmd w<cr>",
    { noremap = true, silent = true, desc = "Save without formatting" }
)

vim.keymap.set("n", "<leader>lw", "<cmd>set wrap!<cr>", { noremap = true, silent = true })

if vim.opt.wrap then
    vim.keymap.set("n", "j", [[v:count ? "j": "gj"]], { expr = true })
    vim.keymap.set("n", "k", [[v:count ? "k": "gk"]], { expr = true })
end

-- Quit all
vim.keymap.set("n", "<leader>qq", "<cmd>quitall<cr>")
vim.keymap.set("n", "<leader>QQ", "<cmd>quitall!<cr>")

-- quick convenient substitute command
vim.keymap.set("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])

-- better indenting
vim.keymap.set("v", "<", "<gv")
vim.keymap.set("v", ">", ">gv")
-- vim.keymap.set("v", "<leader>sr", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])

-- better up/down
vim.keymap.set({ "n", "x" }, "j", "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })
vim.keymap.set({ "n", "x" }, "k", "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })

-- Yank from current position til the then of the current line
vim.keymap.set("n", "Y", "yg$", { noremap = true, silent = true })

-- Double brackets strings are easier to use as they do not require escaping characters --
-- Prevent x and delete key from overriding what is in the clipboard
-- delete single character without copying into register
vim.keymap.set("n", "<del>", [["_x]], { noremap = true, silent = true })

-- Prevent selecting and pasting from overwritting what you originally copied
-- Yank/Copy into clipboard
vim.keymap.set( { "n", "v", "x" }, "<leader>y", [["+y]],
    { noremap = true, silent = false, desc = 'Yank into " register' }
)
-- Delete into the "black hole register"
vim.keymap.set( { "n", "v" }, "<leader>d", [["_d]],
    { noremap = true, silent = false, desc = "Delete into black hole register" }
)

-- Paste from clipboard
-- vim.keymap.set({ "n", "v", "x" }, "<leader>p", [["+p]], { noremap = true, silent = false })
-- vim.keymap.set({ "n", "v", "x" }, "<leader>P", [["+P]], { noremap = true, silent = false })
vim.keymap.set({ "n", "v", "x" }, "<leader>p", [["_dP]], { noremap = true, silent = false })

-- Paste without overwriting register
vim.keymap.set("v", "p", '"_dP')

-- vim.keymap.set("n", "<C-c>", "ciw", { noremap = true, silent = true })

-- duplicate a line and comment out the first line
vim.keymap.set("n", "yc", "yy<cmd>normal gcc<cr>p")

-- always center screen on page-up and page-down
vim.keymap.set("n", "<C-d>", "<C-d>zz", { noremap = true, silent = true })
vim.keymap.set("n", "<C-u>", "<C-u>zz", { noremap = true, silent = true })

-- always center screen when traversing through search results forwards/backwards
vim.keymap.set("n", "n", "nzzzv", { noremap = true, silent = true })
vim.keymap.set("n", "N", "Nzzzv", { noremap = true, silent = true })

-- Search for highlighted text in buffer
vim.keymap.set("v", "//", 'y/<C-R>"<cr>', { desc = "Search for highlighted text" })

-- Move blocks of text
-- vim.keymap.set("n", "<A-j>", "<cmd>m .+1<cr>==", { desc = "Move down" })
-- vim.keymap.set("i", "<A-j>", "<esc><cmd>m .+1<cr>==gi", { desc = "Move down" })
-- vim.keymap.set("n", "<A-k>", "<cmd>m .-2<cr>==", { desc = "Move up" })
-- vim.keymap.set("i", "<A-k>", "<esc><cmd>m .-2<cr>==gi", { desc = "Move up" })
vim.keymap.set("v", "J", ":m '>+1<cr>gv=gv", { desc = "Move block down" })
vim.keymap.set("v", "K", ":m '<-2<cr>gv=gv", { desc = "Move block up" })

-- QuickFix
-- Previous/next quickfix item
vim.keymap.set("n", "]q", "<cmd>cnext<cr>")
vim.keymap.set("n", "[q", "<cmd>cprevious<cr>")

--- Buffers
-- clear TAB mapping before using it
vim.api.nvim_set_keymap("", "<TAB>", "<Nop>", { noremap = true, silent = true })
vim.keymap.set("n", "<leader>q", "<cmd>bdelete<cr>", { noremap = true, silent = true, desc = "Close Buffer" })
vim.keymap.set("n", "<C-x>", ":Bdelete!<CR>", { noremap = true, silent = true, desc = "Close Buffer" }) -- close buffer
-- Close buffer without closing split
--vim.keymap.set("n", "<leader>w", "<cmd>bp|bd #<cr>", { desc = "Close Buffer; Retain Split" })

-- Tabs
vim.keymap.set("n", "<leader>to", "<cmd>tabnew<cr>", { noremap = true, silent = true, desc = "Tab open (new)" })
vim.keymap.set("n", "<leader>tn", "<cmd>tabn<cr>", { noremap = true, silent = true, desc = "Tab next" })
vim.keymap.set("n", "<leader>tp", "<cmd>tabp<cr>", { noremap = true, silent = true, desc = "Tab previous" })
vim.keymap.set("n", "<leader>tx", "<cmd>tabclose<cr>", { noremap = true, silent = true, desc = "Tab close" })
vim.keymap.set("n", "<leader>tX", "<cmd>tabclose!<cr>", { noremap = true, silent = true, desc = "Tab close" })
vim.keymap.set("n", "[T", "<cmd>tabmove -1<cr>", { noremap = true, silent = true, desc = "Tab move left" })
vim.keymap.set("n", "T]", "<cmd>tabmove +1<cr>", { noremap = true, silent = true, desc = "Tab move right" })

-- Splits (windows)
--vim.keymap.set("n", "<leader>sh", "<C-w>s") -- split window horizontally
--vim.keymap.set("n", "<leader>sv", "<C-w>v") -- split window vertically
-- Create Horizontal Split
vim.keymap.set(
    "n",
    "<leader>sh",
    "<cmd>split<cr><C-w>w",
    { noremap = true, silent = true, desc = "Split window horizontally" }
)

-- Create Vertical Split
vim.keymap.set(
    "n",
    "<leader>sv",
    "<cmd>vsplit<cr><C-w>w",
    { noremap = true, silent = true, desc = "Split window vertically" }
)

-- create horizontal split
-- vim.keymap.set(
--     "n",
--     "<leader>wh",
--     "<cmd>split<space>",
--     { noremap = true, silent = true, desc = "Split window horizontally" }
-- )

-- create vertical split
-- vim.keymap.set(
--     "n",
--     "<leader>wv",
--     "<cmd>vsplit<space>",
--     { noremap = true, silent = true, desc = "Split window vertically" }
-- )

-- make split windows equal width & height
vim.keymap.set("n", "<leader>we", "<C-w>=", { noremap = true, silent = true })

-- close current split window
vim.keymap.set("n", "<leader>wx", "<cmd>close<cr>", { noremap = true, silent = true })

-- Easy movement between windows (splits)
--vim.keymap.set("n", "<leader>h", "<cmd>wincmd h<cr>", { noremap = true, silent = true })
--vim.keymap.set("n", "<leader>j", "<cmd>wincmd j<cr>", { noremap = true, silent = true })
--vim.keymap.set("n", "<leader>k", "<cmd>wincmd k<cr>", { noremap = true, silent = true })
--vim.keymap.set("n", "<leader>l", "<cmd>wincmd l<cr>", { noremap = true, silent = true })

vim.keymap.set("n", "<C-h>", "<C-w><C-h>", { noremap = true, silent = true, desc = "move cursor to window left" })
vim.keymap.set("n", "<C-j>", "<C-w><C-j>", { noremap = true, silent = true, desc = "move cursor to window down" })
vim.keymap.set("n", "<C-k>", "<C-w><C-k>", { noremap = true, silent = true, desc = "move cursor to window up" })
vim.keymap.set("n", "<C-l>", "<C-w><C-l>", { noremap = true, silent = true, desc = "move cursor to window right" })

-- Resize with arrows
-- Resize with arrows
-- vim.keymap.set("n", "<A-down>", ":resize +5<cr>", { noremap = true, silent = true, desc = "resize window down" })
vim.keymap.set( "n", "<C-S-Down>", ":resize +5<cr>",
    { noremap = true, silent = true, desc = "Resize Horizontal Split Down" }
)

-- vim.keymap.set("n", "<A-up>", ":resize -5<cr>", { noremap = true, silent = true, desc = "resize window up" })
vim.keymap.set( "n", "<C-S-Up>", ":resize -5<cr>",
    { noremap = true, silent = true, desc = "Resize Horizontal Split Up" }
)

vim.keymap.set( "n", "<C-S-Left>", ":vertical resize -5<cr>",
    { noremap = true, silent = true, desc = "Resize Vertical Split Down" }
)
-- vim.keymap.set(
--     "n",
--     "<A-left>",
--     ":vertical resize -5<cr>",
--     { noremap = true, silent = true, desc = "resize window left" }
-- )

vim.keymap.set( "n", "<C-S-Right>", ":vertical resize +5<cr>",
    { noremap = true, silent = true, desc = "Resize Vertical Split Up" }
)
-- vim.keymap.set(
--     "n",
--     "<A-right>",
--     ":vertical resize +5<cr>",
--     { noremap = true, silent = true, desc = "resize window right" }
-- )

-- Easy navigation in TMUX - needs tmux-navigator plugin
vim.keymap.set("n", "<leader>h", "<cmd>TmuxNavigateLeft<cr>", { noremap = true, silent = true })
vim.keymap.set("n", "<leader>j", "<cmd>TmuxNavigateRight<cr>", { noremap = true, silent = true })
vim.keymap.set("n", "<leader>k", "<cmd>TmuxNavigateDown<cr>", { noremap = true, silent = true })
vim.keymap.set("n", "<leader>l", "<cmd>TmuxNavigateUp<cr>", { noremap = true, silent = true })

-- The callback function receives a table with the following values
-- id: (number) autocmd id
-- event: (string) name of the triggered event |autocmd-events|
-- match: (string) expanded value of |<amatch>|
-- buf: (number) expanded value of |<abuf>|
-- file: (string) expanded value of |<afile>|
-- data: (any) arbitrary data passed |nvim_exec_autocmds()|

-- Highlight Yank
vim.api.nvim_create_autocmd("TextYankPost", {
    desc = "Highlight yanked selection",
    group = vim.api.nvim_create_augroup("HighlightYank", { clear = true }),
    pattern = "*",
    callback = function()
        vim.highlight.on_yank({
            higroup = "IncSearch",
            timeout = 200,
        })
        -- vim.highlight.on_yank()
    end,
})

-- Go to last location when opening a buffer
vim.api.nvim_create_autocmd("BufReadPost", {
    group = vim.api.nvim_create_augroup("LastDocPos", { clear = true }),
    callback = function()
        local mark = vim.api.nvim_buf_get_mark(0, '"')
        local lcount = vim.api.nvim_buf_line_count(0)
        if mark[1] > 0 and mark[1] <= lcount then
            pcall(vim.api.nvim_win_set_cursor, 0, mark)
        end
    end,
})

vim.api.nvim_create_autocmd("FileType", {
    pattern = { "markdown" },
    callback = function()
        -- treesitter-context is buggy with Markdown files
        require("treesitter-context").disable()
    end,
})

-- Start terminal in INSERT mode
-- vim.api.nvim_create_autocmd("TermOpen", {
--     group = "buffer_terminal",
--     pattern = "*",
--     command = "startinsert | set winfixheight"
-- })

-- vim.api.nvim_create_autocmd("FileType", {
--     group = vim.api.nvim_create_augroup("go_autocommands", { clear = true }),
--     pattern = { "go" },
--     callback = function()
--         -- CTRL/control keymaps
--         vim.api.nvim_buf_set_keymap(0, "n", "<C-i>", ":GoImport<CR>", {})
--         vim.api.nvim_buf_set_keymap(0, "n", "<C-b>", ":GoBuild %:h<CR>", {})
--         vim.api.nvim_buf_set_keymap(0, "n", "<C-t>", ":GoTestPkg<CR>", {})
--         vim.api.nvim_buf_set_keymap(0, "n", "<C-c>", ":GoCoverage -p<CR>", {})
--         -- Opens test files
--         vim.api.nvim_buf_set_keymap(0, "n", "A", ":lua require('go.alternate').switch(true, '')<CR>", {}) -- Test
--         vim.api.nvim_buf_set_keymap(0, "n", "V", ":lua require('go.alternate').switch(true, 'vsplit')<CR>", {}) -- Test Vertical
--         vim.api.nvim_buf_set_keymap(0, "n", "S", ":lua require('go.alternate').switch(true, 'split')<CR>", {}) -- Test Split
--     end,
-- })

local M = {}

local function applyCustomColors()
    -- vim.notify("InsideApplyCustomColors", vim.log.levels.INFO)

    -- color palette: https://www.color-hex.com/color-palette/1048018
    --
    -- Custom Colors

    -- local azureblue = "#7dc5e9"
    -- local cornflowerblue = "#63b1ff"
    -- local comment_gray = "#5C6370"
    -- local black = "#1f1f1f"
    local softblack = "#222222"
    -- local softblack = "#333333"
    -- local softblack2 = "#3c3836"
    -- local softblack = "#333366"
    local softwhite = "#b0bec5"
    -- local cyan = "#56b6c2"
    -- local lightcyan = "#82d5e6"
    -- local orange = "#e6a37c"
    -- local red = "#ed8695"
    -- local red = "#d0021b"
    -- local green = "#98C379"
    -- local green = "#00ff9f"
    -- local brightgreen = "#ccff00"
    -- local springgreen = "#64e880"
    -- local mossgreen = "#a4dc99"
    -- local pastelgreen = "#c2f5bf"
    -- local softaqua = "#8bc9cc"
    -- local softturqoise = "#8ad4c9"
    -- local brightyellow = "#ffff00"
    -- local brightyellow = "#ffff33"
    local brightyellow = "#f8ff0f"
    -- local paleyellow = "#e6e70e"
    -- local smoothyellow = "#f5a623"

    -- local yellow0 = "#fdd835"
    -- local yellow1 = "#e5c07b"
    -- local yellow2 = "#fadb2f"
    -- local diff_yellow = "#d19a66"
    local markeryellow = "#ffcc33"
    -- local neonyellow = "#fcf279"
    local turquoise = "#5cfff7"
    -- local purple = "#633c6e"
    -- local dim_purple = "#c678dd"
    -- local blue = "#0d4c7f"
    -- local dim_blue = "#61afef"
    -- local lavenderblue = "#93aff5"
    -- local skyblue = "#51c7fc"
    -- local softblue = "#a0d2fa"
    -- local gutter_gray = "#4b5263"
    -- local grey0 = "#cdd1f4"
    -- local grey1 = "#b8c0e1"
    -- local grey2 = "#a4aecc"
    -- local grey3 = "#969bb8"
    -- local grey4 = "#8288a3"
    -- local grey5 = "#918d87"
    -- local grey6 = "#6f758c"
    -- local darkgrey0 = "#51586b"
    -- local darkgrey1 = "#61677f"
    -- local darkgrey2 = "#52586e"
    -- local darkgrey3 = "#42495e"
    -- local darkgrey4 = "#3b3f54"
    -- local darkgrey5 = "#353b4e"

    -- set transparent background
    -- vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
    -- vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" })

    -- set the color of the line number
    vim.api.nvim_set_hl(0, "LineNr", { fg = softwhite })
    -- vim.api.nvim_set_hl(0, "LineNrAbove", { fg = "#ffffff", bold = true })
    -- vim.api.nvim_set_hl(0, "LineNrBelow", { fg = "#737373", bold = false })

    -- set the color of the line number of the cursor line
    vim.api.nvim_set_hl(0, "CursorLineNr", { fg = turquoise })

    -- set colors when we yank
    vim.api.nvim_set_hl(0, "IncSearch", { fg = softblack, bg = markeryellow })

    -- vim.api.nvim_set_hl(0, "LineNrAbove", { fg = "#6e738d", bold = true })
    -- vim.api.nvim_set_hl(0, "LineNr", { fg = "#b4befe", bold = true })
    -- vim.api.nvim_set_hl(0, "LineNrBelow", { fg = "#6e738d", bold = true })
    -- highlight TabLine ctermbg=none
    -- highlight TabLineFill ctermbg=none
    -- highlight TabLineSel ctermbg=none
    -- vim.api.nvim_set_hl(0, "TabLine", { fg = softblack, bg = markeryellow })
    -- vim.api.nvim_set_hl(0, "TabLineSel", { fg = softblack, bg = markeryellow })
    -- vim.api.nvim_set_hl(0, "TabLineFill", { fg = softblack, bg = markeryellow })

    -- Highlight bookmark icons
    vim.api.nvim_set_hl(0, "MarkSignHL", { fg = brightyellow })
    vim.api.nvim_set_hl(0, "MarkSignNumHL", { fg = brightyellow })
end

local function applyGruvboxMaterial()
    vim.g.gruvbox_terminal_colors = 1
    vim.g.gruvbox_better_performance = 1
    vim.g.gruvbox_material_background = "hard"     -- hard medium soft
    vim.g.gruvbox_material_foreground = "original" -- material mix original
    vim.g.gruvbox_material_enable_bold = 1         -- 0,1
    vim.g.gruvbox_material_enable_italic = 1       -- 0,1
    vim.g.gruvbox_material_ui_contrast = "high"    -- low, high
    vim.g.gruvbox_material_transparent_background = 2 -- 0,1,2
    vim.cmd.colorscheme("gruvbox-material")
end

local function applyCatppuccin()
    local cat_ok, catppuccin = pcall(require, "catppuccin")

    if not cat_ok then
        return
    end

    catppuccin.setup({
        flavour = "mocha",
        transparent_background = false,
        integrations = {
            aerial = true,
            alpha = true,
            barbar = true,
            cmp = true,
            dap = true,
            dap_ui = true,
            harpoon = true,
            illuminate = {
                enabled = true,
                lsp = true,
            },
            gitsigns = true,
            nvimtree = true,
            navic = {
                enabled = false,
                custom_bg = "NONE",
            },
            mason = true,
            treesitter = true,
            telescope = {
                enabled = true,
            },
            ufo = true,
        },
    })

    vim.cmd.colorscheme("catppuccin-mocha")
end

local function applyEverforest()
    vim.g.everforest_terminal_colors = 1
    vim.g.everforest_enable_italic = 1
    vim.g.everforest_cursor = "aqua"
    vim.g.everforest_background = "hard" -- hard, medium(default), soft
    vim.g.everforest_better_performance = 1
    vim.cmd.colorscheme("everforest")
end

local function applyNordic()
    local nordic_ok, nordic = pcall(require, "nordic")

    if not nordic_ok then
        return
    end

    nordic.setup({
        -- This callback can be used to override the colors used in the palette.
        on_palette = function(palette)
            return palette
        end,
        -- Enable bold keywords.
        bold_keywords = false,
        -- Enable italic comments.
        italic_comments = true,
        -- Enable general editor background transparency.
        transparent_bg = false,
        -- Enable brighter float border.
        bright_border = false,
        -- Reduce the overall amount of blue in the theme (diverges from base Nord).
        reduced_blue = true,
        -- Swap the dark background with the normal one.
        swap_backgrounds = false,
        -- Override the styling of any highlight group.
        override = {},
        -- Cursorline options.  Also includes visual/selection.
        cursorline = {
            -- Bold font in cursorline.
            bold = false,
            -- Bold cursorline number.
            bold_number = true,
            -- Available styles: 'dark', 'light'.
            theme = "dark",
            -- Blending the cursorline bg with the buffer bg.
            blend = 0.85,
        },
        noice = {
            -- Available styles: `classic`, `flat`.
            style = "classic",
        },
        telescope = {
            -- Available styles: `classic`, `flat`.
            style = "flat",
        },
        leap = {
            -- Dims the backdrop when using leap.
            dim_backdrop = false,
        },
        ts_context = {
            -- Enables dark background for treesitter-context window
            dark_background = true,
        },
    })

    nordic.load()
    vim.cmd.colorscheme("nordic")
end

local function applyMaterial()
    local material_ok, material = pcall(require, "material")

    if not material_ok then
        return
    end

    -- https://github.com/marko-cerovac/material.nvim?tab=readme-ov-file

    material.setup({

        contrast = {
            terminal = false,   -- Enable contrast for the built-in terminal
            sidebars = false,   -- Enable contrast for sidebar-like windows ( for example Nvim-Tree )
            floating_windows = false, -- Enable contrast for floating windows
            cursor_line = false, -- Enable darker background for the cursor line
            lsp_virtual_text = false, -- Enable contrasted background for lsp virtual text
            non_current_windows = false, -- Enable contrasted background for non-current windows
            filetypes = {},     -- Specify which filetypes get the contrasted (darker) background
        },

        styles = { -- Give comments style such as bold, italic, underline etc.
            comments = { --[[ italic = true ]]
            },
            strings = { --[[ bold = true ]]
            },
            keywords = { --[[ underline = true ]]
            },
            functions = { --[[ bold = true, undercurl = true ]]
            },
            variables = {},
            operators = {},
            types = {},
        },

        plugins = { -- Uncomment the plugins that you use to highlight them
            -- Available plugins:
            -- "coc"
            "dap",
            -- "dashboard",
            -- "eyeliner",
            -- "fidget",
            -- "flash",
            "gitsigns",
            "harpoon",
            -- "hop",
            "illuminate",
            "indent-blankline",
            "lspsaga",
            -- "mini",
            -- "neogit",
            -- "neotest",
            -- "neo-tree",
            -- "neorg",
            -- "noice",
            "nvim-cmp",
            "nvim-navic",
            "nvim-tree",
            "nvim-web-devicons",
            -- "rainbow-delimiters",
            -- "sneak",
            "telescope",
            -- "trouble",
            -- "which-key",
            -- "nvim-notify",
        },

        disable = {
            colored_cursor = false, -- Disable the colored cursor
            borders = false, -- Disable borders between verticaly split windows
            background = false, -- Prevent the theme from setting the background (NeoVim then uses your terminal background)
            term_colors = false, -- Prevent the theme from setting terminal colors
            eob_lines = false, -- Hide the end-of-buffer lines
        },

        high_visibility = {
            lighter = false, -- Enable higher contrast text for lighter style
            darker = false, -- Enable higher contrast text for darker style
        },

        lualine_style = "default", -- Lualine style ( can be 'stealth' or 'default' )

        async_loading = true, -- Load parts of the theme asyncronously for faster startup (turned on by default)

        custom_colors = nil, -- If you want to override the default colors, set this to a function

        custom_highlights = {}, -- Overwrite highlights with your own
    })
    --
    -- different styles: darker, lighter, oceanic, palenight, deep ocean
    vim.g.material_style = "oceanic"
    vim.cmd.colorscheme("material")
end

function M.ApplyTheme(color_theme)
    -- make sure we have a (default) color theme
    local color_scheme = color_theme or "gruvbox"

    -- set the background
    vim.opt.termguicolors = true
    vim.opt.background = "dark"

    if color_scheme == "everforest" then
        applyEverforest()
    elseif color_theme == "gruvbox_material" then
        applyGruvboxMaterial()
    elseif color_scheme == "catppuccin" then
        applyCatppuccin()
    elseif color_scheme == "material" then
        applyMaterial()
    elseif color_scheme == "nordic" then
        applyNordic()
    else
        -- apply the color theme
        vim.cmd.colorscheme(color_scheme)
    end

    --vim.cmd.colorscheme("tonyonight")  -- tokyonight,  tokyonight-night,  tokyonight-storm,  tokyonight-day,  tokyonight-moon
    --vim.cmd.colorscheme("catppuccin")  -- catppuccin-frappe,  catppuccin-mocha,  catppuccin-macchiato
    -- material different styles:        -- darker, lighter, oceanic, palenight, deep ocean

    applyCustomColors()
end

return M

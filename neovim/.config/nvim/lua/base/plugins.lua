local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end

vim.opt.rtp:prepend(lazypath)

local plugins = {
    -- Color Themes
    { "fynnfluegge/monet.nvim",      name = "monet",            priority = 1000 },
    { "catppuccin/nvim",             name = "catppuccin",       priority = 1000 },
    { "navarasu/onedark.nvim",       name = "onedark",          priority = 1000 },
    { "folke/tokyonight.nvim",       name = "tokyonight",       priority = 1000 },
    { "sainnhe/everforest",          name = "everforest",       priority = 1000 },
    { "gruvbox-community/gruvbox",   name = "gruvbox",          priority = 1000 },
    { "sainnhe/gruvbox-material",    name = "gruvbox-material", priority = 1000 },
    { "marko-cerovac/material.nvim", name = "material",         priority = 1000 },
    { "maxmx03/solarized.nvim",      name = "solarized",        priority = 1000 },
    { "EdenEast/nightfox.nvim",      name = "nightfox",         priority = 1000 },
    { "masisz/ashikaga.nvim",        name = "ashikaga",         priority = 1000 },

    -- install icons used by several plugins
    {
        "nvim-tree/nvim-web-devicons",
    },

    -- syntax parser
    {
        "nvim-treesitter/nvim-treesitter",
        dependencies = "nvim-treesitter/nvim-treesitter-textobjects",
    },
    {
        "RRethy/nvim-treesitter-textsubjects",
        dependencies = "nvim-treesitter/nvim-treesitter",
    },
    {
        "nvim-treesitter/nvim-treesitter-context",
        opts = {
            enable = true,
            mode = "topline",
            line_numbers = true,
        },
    },
    {
        "nvim-treesitter/nvim-treesitter-refactor",
        dependencies = "nvim-treesitter/nvim-treesitter",
    },

    -- cursor different modes
    -- {
    --     "mvllow/modes.nvim",
    --     tag = "v0.2.0",
    -- },

    -- {
    --     "svampkorg/moody.nvim",
    --     event = { "ModeChanged" },
    --     dependencies = {
    --         "catppuccin/nvim",
    --     },
    -- },

    -- Dashboard. Initial page
    {
        "goolord/alpha-nvim",
        event = "VimEnter",
        requires = {
            "nvim-tree/nvim-web-devicons",
            "nvim-lua/plenary.nvim",
        },
    },

    -- Autopairs, integrates with both cmp and treesitter
    {
        "windwp/nvim-autopairs",
        config = function()
            require("nvim-autopairs").setup({})
        end,
    },

    -- Add indentation guides even on blank lines
    {
        "lukas-reineke/indent-blankline.nvim",
        main = "ibl",
        config = function()
            require("ibl").setup()
        end,
    },

    -- Easy Comment stuff:  "gc" to comment visual regions/lines
    {
        "numToStr/Comment.nvim",
        lazy = false,
    },

    -- Surround Stuff
    --{ "tpope/vim-surround" },
    {
        "kylechui/nvim-surround",
        dependencies = {
            "nvim-treesitter/nvim-treesitter",
            "nvim-treesitter/nvim-treesitter-textobjects",
        },
        event = "VeryLazy",
        -- version = "2.1.7",
        config = function()
            require("nvim-surround").setup()
        end,
    },

    -- Fold management
    {
        "kevinhwang91/nvim-ufo",
        dependencies = "kevinhwang91/promise-async",
    },

    -- Telescope: Fuzzy Finder
    {
        "nvim-telescope/telescope.nvim",
        dependencies = "nvim-lua/plenary.nvim",
    },
    {
        "nvim-telescope/telescope-file-browser.nvim",
        dependencies = {
            "nvim-telescope/telescope.nvim",
            "nvim-lua/plenary.nvim",
        },
    },
    {
        "nvim-telescope/telescope-fzf-native.nvim",
        build = "make",
        cond = vim.fn.executable("make") == 1,
        dependencies = "nvim-telescope/telescope.nvim",
    },
    {
        "nvim-telescope/telescope-ui-select.nvim",
        dependencies = "nvim-telescope/telescope.nvim",
    },
    {
        "matkrin/telescope-spell-errors.nvim",
        dependencies = "nvim-telescope/telescope.nvim",
    },

    -- file system tree
    {
        "nvim-tree/nvim-tree.lua",
        dependencies = {
            "nvim-tree/nvim-web-devicons",
        },
    },

    -- file system browsing
    {
        "stevearc/oil.nvim",
        opts = {},
        dependencies = {
            "nvim-tree/nvim-web-devicons",
        },
    },

    -- Buffer management
    {
        "akinsho/bufferline.nvim",
        dependencies = {
            "moll/vim-bbye",
            "nvim-tree/nvim-web-devicons",
        },
    },

    {
        "swaits/scratch.nvim",
        lazy = true,
        cmd = {
            "Scratch",
            "ScratchSplit",
        },
        opts = {},
    },

    -- statusline
    {
        "nvim-lualine/lualine.nvim",
        event = { "VimEnter" },
        requires = {
            "nvim-tree/nvim-web-devicons",
        },
    },

    -- smooth scrolling
    -- {
    --     "karb94/neoscroll.nvim",
    --     config = function()
    --         require("neoscroll").setup({})
    --     end,
    -- },

    -- Register management
    -- {
    --     "tversteeg/registers.nvim",
    --     name = "registers",
    --     config = function()
    --         local registers = require("registers")
    --         registers.setup({})
    --     end,
    -- },

    -- Marks Support
    {
        "chentoast/marks.nvim",
        name = "marks",
        lazy = false,
        event = { "BufReadPre" },
    },

    -- Status Column
    {
        "luukvbaal/statuscol.nvim",
        dependencies = {
            "mfussenegger/nvim-dap",
            "lewis6991/gitsigns.nvim",
        },
        config = function()
            require("statuscol").setup()
        end,
    },

    -- file management
    {
        "ThePrimeagen/harpoon",
        branch = "harpoon2",
        lazy = false,
        dependencies = {
            "nvim-lua/plenary.nvim",
        },
    },

    -- GIT Support --
    {
        "lewis6991/gitsigns.nvim",
        config = function()
            require("gitsigns").setup()
        end,
    },

    {
        "tpope/vim-fugitive",
    },
    -- { "tpope/vim-rhubarb" },

    {
        "mbbill/undotree",
    },

    -- LSP Configuration & Plugins
    {
        "VonHeikemen/lsp-zero.nvim",
        branch = "v3.x",
        dependencies = {
            -- LSP Support
            { "williamboman/mason.nvim" },
            { "neovim/nvim-lspconfig" }, -- Required
            { "williamboman/mason-lspconfig.nvim" },
            { "WhoIsSethDaniel/mason-tool-installer.nvim" },
            {
                "j-hui/fidget.nvim",
                event = { "BufReadPre " },
                config = function()
                    require("fidget").setup()
                end,
            }, -- Optional:LSP useful status updates

            -- Autocompletion
            { "hrsh7th/nvim-cmp" }, -- Required. Completion engine
            { "hrsh7th/cmp-nvim-lsp" }, -- Required
            { "onsails/lspkind.nvim" }, -- Optional. VSCode pictograms
            { "hrsh7th/cmp-buffer" }, -- source for text inbuffer
            { "hrsh7th/cmp-path" }, -- source for file system paths in commands
            { "hrsh7th/cmp-cmdline" }, -- cmdline completions
            { "hrsh7th/cmp-nvim-lua" }, -- Optional. source for lua autocompletion

            -- Snippets
            { "L3MON4D3/LuaSnip" },    -- Required. Snippet engine
            { "saadparwaiz1/cmp_luasnip" }, -- source for lua autocompletion
            { "rafamadriz/friendly-snippets" }, -- snippets library
        },
    },

    -- Notifications
    {
        "folke/trouble.nvim",
        cmd = { "TroubleToggle", "Trouble" },
        requires = {
            "kyazdani42/nvim-web-devicons",
        },
        opts = { use_diagnostic_signs = true },
        config = function()
            require("trouble").setup({})
        end,
    },

    -- Golang Support
    {
        "ray-x/go.nvim",
        event = { "CmdlineEnter" },
        ft = { "go", "gomod" },
        dependencies = {
            "ray-x/guihua.lua",
            "neovim/nvim-lspconfig",
            "nvim-treesitter/nvim-treesitter",
        },
        config = function()
            require("go").setup({})
        end,
    },

    --{
    --    "ray-x/navigator.lua",
    --    dependencies = {
    --        "nvim-treesitter/nvim-treesitter",
    --        { "ray-x/guihua.lua", run = "cd lua/fzy && make" },
    --        { -- Show function signature when you type
    --            "ray-x/lsp_signature.nvim",
    --            event = "VeryLazy",
    --            opts = {},
    --            config = function(_, opts)
    --                require("lsp_signature").setup({})
    --            end,
    --        },
    --    },
    --},

    -- Debug Adapter Protocol - DAP
    {
        "mfussenegger/nvim-dap",
        dependencies = {
            {
                "rcarriga/nvim-dap-ui",
                dependencies = {
                    "mfussenegger/nvim-dap",
                    "nvim-neotest/nvim-nio",
                },
            },
            "jay-babu/mason-nvim-dap.nvim",
            "theHamsta/nvim-dap-virtual-text",
            "nvim-telescope/telescope-dap.nvim",
            "mfussenegger/nvim-dap",
            -- "mfussenegger/nvim-jdtls",
            -- "mfussenegger/nvim-dap-python",
            {
                "leoluz/nvim-dap-go",
                ft = "go",
                config = function(_, opts)
                    require("dap-go").setup(opts)
                end,
            },
        },
    },

    -- Testing
    --  {
    --   "nvim-neotest/neotest",
    --   dependencies = {
    --     {
    --       "vim-test/vim-test",
    --       event = { "BufReadPre" },
    --       config = function()
    --         require("config.test").setup()
    --       end,
    --     },
    --     {"nvim-lua/plenary.nvim"},
    --     {"nvim-treesitter/nvim-treesitter"},
    --     {
    --        "nvim-neotest/neotest-vim-test",
    --        module = { "neotest-vim-test" }
    --     },
    --     {
    --        "nvim-neotest/neotest-python",
    --        module = { "neotest-python" }
    --     },
    --     {
    --        "nvim-neotest/neotest-plenary",
    --        module = { "neotest-plenary" }
    --     },
    --     {
    --        "nvim-neotest/neotest-go",
    --        module = { "neotest-go" }
    --     },
    --     {
    --        "haydenmeade/neotest-jest",
    --        module = { "neotest-jest" }
    --     },
    --     {
    --        "rouge8/neotest-rust",
    --        module = { "neotest-rust" }
    --     },
    --   },
    --   config = function()
    --     require("config.neotest").setup()
    --   end,
    -- },

    -- formatting
    {
        "stevearc/conform.nvim",
        event = { "BufReadPre", "BufNewFile" },
    },

    -- linting
    {
        "mfussenegger/nvim-lint",
        lazy = true,
        -- event = { "BufReadPre", "BufNewFile" }, -- to disable, comment this out
    },

    {
        "RRethy/vim-illuminate",
        lazy = false,
        config = function()
            require("illuminate").configure({})
        end,
    },

    -- code outline window for skimming and quick navigation
    {
        "hedyhli/outline.nvim",
        lazy = true,
        cmd = { "Outline", "OutlineOpen" },
    },

    -- fuzzy picker, LSP symbol navigator + more
    -- {
    --     "bassamsdata/namu.nvim",
    -- },

    -- https://github.com/Isrothy/neominimap.nvim
    -- https://github.com/ptdewey/yankbank-nvim

    -- winbar
    {
        "SmiteshP/nvim-navic",
        requires = { "neovim/nvim-lspconfig" },
    },

    -- Markdown Support
    {
        "MeanderingProgrammer/render-markdown.nvim",
        enabled = true,
        dependencies = {
            "nvim-treesitter/nvim-treesitter",
            "nvim-tree/nvim-web-devicons",
        },
        opts = {},
    },
    {
        "iamcco/markdown-preview.nvim",
        keys = {
            {
                "<leader>mp",
                ft = "markdown",
                "<cmd>MarkdownPreviewToggle<cr>",
                desc = "Markdown Preview",
            },
        },
    },
    {
        "lukas-reineke/headlines.nvim",
        ft = { "markdown", "org", "neoorg" },
        dependencies = { "nvim-treesitter/nvim-treesitter" },
    },
    {
        "bullets-vim/bullets.vim",
    },

    -- terminal
    {
        "akinsho/toggleterm.nvim",
        version = "*",
        config = true,
    },

    -- TMUX Navigation Support
    {
        "christoomey/vim-tmux-navigator",
        lazy = false,
    },
}

local opts = {}

require("lazy").setup(plugins, opts)

vim.api.nvim_set_keymap("n", "<leader>zz", "<cmd>Lazy<cr>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<leader>mm", "<cmd>Mason<cr>", { noremap = true, silent = true })

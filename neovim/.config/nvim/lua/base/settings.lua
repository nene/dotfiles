-- local icons = require("base.icons")

----- Encoding -----
vim.o.encoding = "utf-8"
vim.o.fileencoding = "utf-8"
vim.opt.fileencoding = "utf-8"
vim.scriptencoding = "utf-8"

----- Set Spelling -----
vim.opt.spelllang = "en_us"
vim.opt.spell = true

----- Timeout Settings -----
vim.o.timeout = false
vim.opt.timeoutlen = 300
vim.opt.updatetime = 250 -- Decrease update time faster completion

----- Color Support -----
vim.opt.termguicolors = true

----- Mouse Support -----
vim.opt.mouse = "a" -- Enable mouse mode. Allow the mouse to be used in neovim

----- Cursor -----
-- https://vim.fandom.com/wiki/Configuring_the_cursor
--vim.opt.guicursor = ""  -- always a fat cursor
-- vim.opt.guicursor = "n-v-c:block-Cursor"
--vim.opt.guicursor = "n-v-c:block-Cursor,a:blinkwait5-blinkon5-blinkoff5"
--
-- Working
vim.opt.guicursor = {
    "n-v-c:block",                               -- Normal, visual, command-line: block cursor
    "i-ci-ve:ver25",                             -- Insert, command-line insert, visual-exclude: vertical bar cursor with 25% width
    "r-cr:hor20",                                -- Replace, command-line replace: horizontal bar cursor with 20% height
    "o:hor50",                                   -- Operator-pending: horizontal bar cursor with 50% height
    "a:blinkwait700-blinkoff400-blinkon250",     -- All modes: blinking settings
    "sm:block-blinkwait175-blinkoff150-blinkon175", -- Showmatch: block cursor with specific blinking settings
}

-- vim.opt.guicursor = "n-v-c:block-Cursor,i:block-CursorInsert"

vim.opt.cursorline = true -- highlight the current line

----- Ruler Options -----
vim.opt.ruler = true -- show cursor line and column in status

----- Line numbers -----
vim.opt.number = true         -- Make line numbers default
vim.opt.relativenumber = true -- set relative line numbers
vim.opt.numberwidth = 4       -- set number column width to 2 {default 4}
vim.opt.colorcolumn = "80"
vim.opt.signcolumn = "yes"    -- always show the sign column, otherwise it would shift the text each time

----- Tab Settings -----
vim.opt.expandtab = true -- convert tabs to spaces
vim.opt.tabstop = 4      -- number of inserted spaces for a tab
vim.opt.softtabstop = 4  -- number of space tabs count for while editing
vim.opt.shiftwidth = 4   -- number of spaces inserted for each indentation

----- Indentation Settings ------- size of an indent
vim.opt.autoindent = true  -- auto indent the newline
vim.opt.breakindent = true -- enable break indent
vim.opt.shiftround = true  -- Round indent to multiples of 'shiftwidth'
vim.opt.smartindent = true

----- Splits Settings -----
vim.opt.splitbelow = true -- force all horizontal splits to go below current window
vim.opt.splitright = true -- force all vertical splits to go to the right of current window

-- display lines as one long line
vim.opt.wrap = false

----- Splits Settings -----
vim.opt.splitbelow = true         -- force all horizontal splits to go below current window
vim.opt.splitright = true         -- force all vertical splits to go to the right of current window

vim.opt.virtualedit = "block"     -- Allow cursor to move where there is no text in visual block mode
vim.opt.writebackup = false       -- if a file is being edited by another program (or was written to file while editing with another program), it is not allowed to be edited
vim.opt.clipboard = "unnamedplus" -- allows neovim to access the system clipboard
vim.opt.backup = false
vim.opt.undofile = true           -- Save undo history. Enable persistent undo
vim.opt.swapfile = false          -- creates a swapfile
--vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"   -- undo file dir location

----- Search Options -----
vim.opt.smartcase = true  -- Don't ignore case with capitals
vim.opt.hlsearch = true   -- Set highlight on search. Highlight all matches
vim.opt.ignorecase = true -- Case insensitive searching
vim.opt.incsearch = true  -- Set incremental search

----- Popup Menu Settings -----
-- Set completeopt to have a better completion experience, mostly just for cmp
vim.opt.completeopt = "menu,menuone,noselect"
vim.opt.pumblend = 10  -- popup blend
vim.opt.pumheight = 10 -- popup menu height

-- Command Line
vim.opt.cmdheight = 1                  -- more space in the neovim command line for displaying messages
vim.opt.wildmode = "longest:full,full" -- Command-line completion mode

vim.opt.shortmess:append("c")          -- Completion messages
vim.opt.shortmess:append("I")          -- No version splash

-----  Display Settings -----
vim.opt.scrolloff = 5     -- number of line before or after cursor and end of screen
vim.opt.sidescrolloff = 5 -- number of spaces before or after at end of line. Vertically

vim.opt.conceallevel = 2

-----  Folding Settings -----
vim.opt.foldcolumn = "1"
vim.opt.foldenable = true
vim.opt.foldlevel = 99 -- using ufo need a large value (we can decrease)
vim.opt.foldlevelstart = 99

-----  Marks Settings -----
-- vim.g.bookmark_sign = icons.ui.Flag

-----  Netrw File Browser Settings -----
-- Better Netrw
--vim.g.netrw_banner = 0 -- Hide banner
--vim.g.netrw_altv = 1   -- Open with right splitting
--vim.g.netrw_liststyle = 3 -- Tree-style view
--vim.g.netrw_browse_split = 4 -- Open in previous window
--vim.g.netrw_list_hide = (vim.fn["netrw_gitignore#Hide"]()) .. [[,\(^\|\s\s\)\zs\.\S\+]] -- use .gitignore
--vim.g.netrw_winsize = 25
--vim.g.netrw_browser_split = 0

----- Special Character Settings -----
-- tab = "▸",
-- tab = "⤷ ",
-- tab = "→ ",
-- trail = "·"
-- trail = "•",
-- nbsp = "·"
-- nbsp = "␣",
-- space = "·",
-- eol = "↴",
-- trail = "⸱", -- Word Separator Middle Dot

-- nbsp = "·",
-- vim.opt.listchars = { tab = '» ', trail = '·', nbsp = '␣' }
vim.opt.list = true -- show more invisible characters
-- vim.opt.listchars = {
--     nbsp = "·",
--     space = "·",
--     trail = "▫",
--     tab = "▸ ",
--     eol = "↴",
--     extends = "»",
--     precedes = "«",
-- }

vim.opt.listchars = {
    nbsp = "⦸", -- CIRCLED REVERSE SOLIDUS (U+29B8, UTF-8: E2 A6 B8)
    space = "·",
    extends = "»", -- RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK (U+00BB, UTF-8: C2 BB)
    precedes = "«", -- LEFT-POINTING DOUBLE ANGLE QUOTATION MARK (U+00AB, UTF-8: C2 AB)
    trail = "▫",
    tab = "▸ ",
    eol = "↴",
}
vim.opt.showbreak = "↳ " -- DOWNWARDS ARROW WITH TIP RIGHTWARDS (U+21B3, UTF-8: E2 86 B3)
-- =================================================================

--opt.fillchars = {
--    foldopen = "",
--    foldclose = "",
--    fold = " ",
--    foldsep = " ",
--    diff = "╱",
--    eob = " ",
--}
--opt.foldlevel = 99999
--opt.foldtext = "v:lua.require'lazyvim.util.ui'.foldtext()"
--opt.foldmethod = "expr"
--opt.foldexpr = "nvim_treesitter#foldexpr()"

-- =================================================================
-- Neovide Settings along with guifont
vim.opt.guifont = "FiraCode Nerd Font Mono:h15.5"
-- vim.opt.guifont="Inconsolata Nerd Font Mono:h8.5"
-- vim.opt.guifont = "Iosevka:h18.5"
vim.cmd([[let g:neovide_cursor_animation_length=0.10]])
vim.cmd([[let g:neovide_cursor_trail_length=0.5]])
vim.cmd([[let g:neovide_remember_window_size = v:true]])
vim.cmd([[let g:neovide_cursor_antialiasing=v:true]])
--vim.cmd [[let g:neovide_cursor_vfx_mode="pixiedust"]]
vim.cmd([[let g:neovide_cursor_vfx_opacity=200.0]])
vim.cmd([[let g:neovide_cursor_vfx_particle_lifetime=1.2]])
vim.cmd([[let g:neovide_cursor_vfx_particle_density=9.0]])
vim.cmd([[let g:neovide_cursor_vfx_particle_speed=10.0]])

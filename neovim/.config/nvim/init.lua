require("base")

local colorscheme = require("base.colorscheme")
colorscheme.ApplyTheme("gruvbox-material") -- default is gruvbox

-- Available themes
-- ashikaga
-- catppuccin
-- everforest
-- gruvbox
-- gruvbox-material
-- material
-- monet
-- nordic
-- nightfox
-- onedark
-- tokyonight

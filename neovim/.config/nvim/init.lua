require("base")

local colorscheme = require("base.colorscheme")
colorscheme.ApplyTheme("everforest") -- default is gruvbox

-- Available themes
-- monet
-- catppuccin
-- onedark
-- caret
-- nordic
-- nightfox
-- material
-- tokyonight
-- everforest
-- gruvbox
-- gruvbox-baby
-- gruvbox-material

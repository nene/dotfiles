# ===============================================
#|_   _| __ ___  _   ___  __
#  | || '_ ` _ \| | | \ \/ /
#  | || | | | | | |_| |>  <
#  |_||_| |_| |_|\__,_/_/\_\
# ===============================================

## Unbind default prefix key
unbind C-b

## Set the prefix globally. Remap from C-b to C-a
#set -g prefix C-a
#set-option -g prefix C-a
#bind C-a send-prefix
#bind-key C-a send-prefix

## Set the prefix globally. Remap from C-b to C-Space
set-option -g prefix C-Space
bind C-Space send-prefix
bind-key C-Space send-prefix

# Default shell
#set-option -g default-command /usr/bin/zsh
#set-option -g default-shell /usr/bin/zsh



# Decrease wait time when ESC is pressed to 20ms
set -g escape-time 20

# Set command history size in the buffer
set -g history-limit 10000

# Using VIM inside TMUX.
# Fix delay time between tmux an vim.
# Do not wait after hitting ESC before it responds
set -sg escape-time 0

# Make messages stay on the screen for 2 seconds
set -g display-time 1000
set-option -g display-panes-time 2000

# Enable mouse support
set -g mouse on

# Turn on mouse support
set-option -g -q mode-mouse on


####################
# COPY AND PASTE
####################
#bind-key -t vi-copy WheelUpPane halfpage-up
#bind-key -t vi-copy WheelDownPane halfpage-down
#bind-key . copy-mode
#bind-key -T copy-mode-vi v send -X begin-selection
#bind-key -T copy-mode-vi send -X copy-selection
#unbind p
#bind p paste-buffer



####################
# WINDOWS AND PANES
####################

# Create new window
bind-key c new-window -c "#{pane_current_path}"

# Close a window
bind X confirm kill-window

# Display pane numbers
bind-key d display-panes

# Start windows at index 1 and not 0
set -g base-index 1

# Start panes at index 1 and not 0
setw -g pane-base-index 1

# Ensure winow index numbers get reordered on delete
set-option -g renumber-windows on


# Set activity notification - disabled for the moment
#setw -g monitor-activity on
#set -g visual-activity on

# Disable window automatic rename
set -g automatic-rename off
setw -g automatic-rename off
set-window-option -g automatic-rename off
# Ensure window titles get renamed automatically
#setw -g automatic-rename

# Swap windows - change window index
# mode the current windows to be the first window
bind-key T swap-window -t 1

# Control-Shift-Left move the current window to the left position
bind-key -n C-S-Left swap-window -t -1

# Control-Shift-Left move the current window to the left position
bind-key -n C-S-Right swap-window -t +1


## unbind default pane split keys
unbind '"'
unbind %

## Set key-bindings to create panes

## horiontal split (default prefix %)
#bind-key v split-window -v -c "#{pane_current_path}"

## vertical split (default prefix ")
#bind-key h split-window -h -c "#{pane_current_path}"

bind-key - split-window -v -c "#{pane_current_path}"
bind-key | split-window -h -c "#{pane_current_path}"


## Set keys to move around panes.
# Ommit the prefix with -n and use the ALT (meta) instead
#bind -n M-h select-pane -L
#bind -n M-j select-pane -D
#bind -n M-k select-pane -U
#bind -n M-l select-pane -R

# Alternate way of moving between panes. Use ALT + arrow keys
bind -n M-Left  select-pane -L
bind -n M-Down  select-pane -D
bind -n M-Up    select-pane -U
bind -n M-Right select-pane -R

# Smart pane switching with awareness of Vim splits.
# See: https://github.com/christoomey/vim-tmux-navigator
is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
    | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"
bind-key -n 'C-h' if-shell "$is_vim" 'send-keys M-Left'  'select-pane -L'
bind-key -n 'C-j' if-shell "$is_vim" 'send-keys M-Down'  'select-pane -D'
bind-key -n 'C-k' if-shell "$is_vim" 'send-keys M-Up'  'select-pane -U'
bind-key -n 'C-l' if-shell "$is_vim" 'send-keys M-Right'  'select-pane -R'
tmux_version='$(tmux -V | sed -En "s/^tmux ([0-9]+(.[0-9]+)?).*/\1/p")'
if-shell -b '[ "$(echo "$tmux_version < 3.0" | bc)" = 1 ]' \
    "bind-key -n 'C-\\' if-shell \"$is_vim\" 'send-keys C-\\'  'select-pane -l'"
if-shell -b '[ "$(echo "$tmux_version >= 3.0" | bc)" = 1 ]' \
    "bind-key -n 'C-\\' if-shell \"$is_vim\" 'send-keys C-\\\\'  'select-pane -l'"

bind-key -T copy-mode-vi 'M-Left' select-pane -L
bind-key -T copy-mode-vi 'M-Down' select-pane -D
bind-key -T copy-mode-vi 'M-Up' select-pane -U
bind-key -T copy-mode-vi 'M-Right' select-pane -R
bind-key -T copy-mode-vi 'M-\' select-pane -l


# Resize Panes
bind -r H resize-pane -L 5
bind -r J resize-pane -D 5
bind -r K resize-pane -U 5
bind -r L resize-pane -R 5


# swap windows
bind-key W choose-tree -Zw "swap-window -t '%%'"
bind-key P choose-tree -Zw "swap-pane -t '%%'"
bind-key C-p choose-tree -Zw "move-pane -t '%%'"

bind-key C-M-w command-prompt -p "Swap Current Window To? (e.g 3; 4; session_name:5)" "swap-window -t '%%'"
bind-key C-M-p command-prompt -p "Swap Current Pane To? (e.g 2.0; session_name:4.0)" "swap-pane -t '%%'"
bind-key M-p command-prompt -p "Move Current Pane To? (e.g 3.1; session_name:6.0)" "move-pane -t '%%'"

####################
# SYNCRONIZE-PANES
# enables syncronize-panes toggling
# Set pane synchronization of keys. Ommit prefix with -n
#bind -n C-s setw synchronize-panes
####################

# Uppercase Turns Sync on
bind s setw synchronize-panes on \; display-message "Panel Sync on"

# Lowercase Turns Sync off
bind S setw synchronize-panes off \; display-message "Panel Sync off"



# rearrange panes
bind-key = select-layout main-horizontal
bind-key + select-layout main-vertical


# Window rotation. Ommit prefix with -n
bind-key -n C-r rotate-window

# Window switching with vim keys.
# Ommit prefix with -n .In addition to prefix+index; prefix+n; prefix+p
bind -r -n C-h select-window -t :-
bind -r -n C-l select-window -t :+

# Window Swap with arrows
bind S-Up swap-window -t -1       # swap window left 1
bind S-Down swap-window -t +1     # swap window right 1
bind S-Left select-window -t -1   # move window left 1
bind S-Right select-window -t +1  # move window right 1


# dim-out all other windows that are not active
# Create dimming effect we set the foreground text color to lighter grey
# then use darker grey for background color.
# active window, we use black and white
#setw -g window-style fg=colour240,bg=colour235
#setw -g window-active-style fg=white,bg=black

bind-key : command-prompt
bind-key r refresh-client
bind-key L clear-history

#set -g message-style fg=white,bold,bg=black



####################
# Colors
####################

# Enable color support inside tmux
# Set default terminal mode to 256 colors - ensure we're using 256 colors
set-option -g default-terminal "screen-256color"
set -g default-terminal "screen-256color"

# Add truecolor support
set-option -ga terminal-overrides ",xterm-256color:Tc"


# Window
set -g window-status-current-style "fg=colour39,underscore"
#set -g window-status-current-style "bg=red,fg=colour39,underscore"
# active window title colors
#set-window-option -g window-status-current-style fg=colour166,bg=default,bright

# Pane Colors
set -g pane-border-style fg=colour240
set -g pane-active-border-style fg="#00BEBE"


# pane number display
set-option -g display-panes-active-colour colour33 #blue
set-option -g display-panes-colour colour166 #orange

####################
# Status bar
####################

# toggle status Bar
bind-key b  set-option status on
bind-key B  set-option status off

# Set status bar screen position
set -g status-position top

# Set the refresh rate to one second
set-option -g status-interval 1
set -g status-interval 1

# default statusbar colors
##set-option -g status-style bg=colour235,fg=colour136,default
#set -g status-style bg='#44475a',fg='#bd93f9'

#set -g status-bg black
#set -g status-fg white

# default window title colors
#set-window-option -g window-status-style fg=colour244,bg=colour234,dim

#set -g window-status-separator " | "
#set -g status-justify centre

#set -g status-left-length 120
#set -g status-right-length 50



### Status Bar Items
## status left
#set -g status-left "#{?client_prefix,#[bg=red]#[fg=black],}TMUX#[default] [#{session_name}] "
#set -g status-left "#{?client_prefix,#[bg=red]#[fg=black],} TMUX #[fg=black]#[bg=black] "

#set -g status-left "#[bg=black]#[fg=black]#{?client_prefix,#[fg=red]#[bg=black],} TMUX "
##set -g status-left "#[bg=colour8]#[fg=white]#[default][#{session_name}] "



## window list
#set -g window-status-format "#[fg=colour8] #I #[fg=colour231]#W#[fg=colour166]#F "
#set -g window-status-current-format "#[fg=colour117,bg=colour31] #I #[fg=colour231]#W#[fg=colour234]#F "
#set -g window-status-separator ""



## Display a clock on the right of status bar
## TMUX date follows strftime format - https://linux.die.net/man/3/strftime
#set -g status-right "#[fg=cyan]%A, %d %b %Y %I:%M:%S %p"


# Reload config file by the command line
bind-key R source-file ~/.tmux.conf \; display-message "TMUX Configuration Reloaded "

#set-environment -g PATH "/usr/local/bin:/bin:/usr/bin"


# Plugin Support
# TPM - TMUX Plugin Manager
# Initialize TPM - keep this line at the VERY BOTTON of tmux.cond
run -b '~/.tmux/plugins/tpm/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin "arcticicestudio/nord-tmux"
#set -g @plugin 'dracula/tmux'
#set -g @plugin 'egel/tmux-gruvbox'
#set -g @tmux-gruvbox 'dark'



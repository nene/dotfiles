

 ## Install
```

apt-get install -y stow

brew install stow
```


#### specify directories
stow -nvt ~ git nvim alacritty

- n no ... no-op it will not do any modifications
- v verbose
- t target directory to process

> NOTE: stow will not override

stow --dir=~/dotfiles --target=~/

#### everything
stow  -nvt ~ *
stow -n -v -t ~ *


stow --adopt -n -v -t ~ *

"adopt" will "adopt" the changes. It will make directory changes


### Directory Structure
repo dir "~/repos/dotfiles" that contains:
vim/vimrc
zsh/zshrc
config/nvim
config/gcloud
config/alacritty



When "stow" is executed, 
~/.vimrc -> ~/repos/dotfiles/vim/.vimrc
~/.zshrc -> ~/repos/dotfiles/zsh/.zshrc
~/.config/htop -> ../../repos/dotfiles/htop/.config/htoprc

GNU Stow walks the file and directory hierarchy of the directory passed as the
first parameter to the `stow` command and creates symbolic links to theose
files in the equivalent locations in the target directory.

> NOTE: Our "dotfiles" directory `must have the same layout` as where the 
> files would be placed under the home directory.
> This means you will need to have the equivalent subdirectory structure 
> in your dotfiles folder so that all symbolic links get created in the right place.

`stow .` 

is equivalent to

`stow --dir=~/dotfiles --target=~/` OR `stow -d ~/dotfiles -t ~/`



> NOTE: Better to execute with the 'n' flag first, to know what is it going to do: `stow  -vSt ~ *`

#### Create symlinks (everything)
stow  -vSt ~ *

#### Create symlinks (list specific parts)
stow  -vSt ~ git htop zsh

#### Delete symlinks (everything)
stow  -vDt ~ *

#### Delete symlinks (everything)
stow  -vDt ~ git htop zsh

### Flags
"-S" or "--stow"  
Stow packages that follow this option into the target directory.
This is default action, so it can be omitted if you are only stowing packages 
rather than performing a mixture of stow/delete/restow actions

"-D" or "--delete"
Unstow the packages that follow this option from the target directory rather
than installing them

"-R" or "--restow" 
Restow packages (first unstow, then stow again).
Useful fort pruning obsolete symlinks from the target tree after updating the 
software in a packages



## Ignoring files and directories
To skip files, we can create a file in our dotfiles folder called `.stow-local-ignore`

For example
```
\.git
misc
LICENSE
README.md
^/.*\.org
```

## Reference Links
- [systemcrafters.net/managing-your-dotfiles/using-gnu-stow](https://systemcrafters.net/managing-your-dotfiles/using-gnu-stow/)

# Git Links

- [18 git commands - cheat-sheet-software-developer](https://towardsdatascience.com/git-commands-cheat-sheet-software-developer-54f6aedc1c46)
- [5-git-commands-that-you-probably-arent-using-much](https://betterprogramming.pub/5-git-commands-that-you-probably-arent-using-much-5da9a0b95399)
- [git tagging](https://levelup.gitconnected.com/git-tagging-a-brief-guide-1af080b5bb6f)

## Git Config
- [make-gitconfig-work-for-you](http://michaelwales.com/articles/make-gitconfig-work-for-you/)
- [configuring-git](https://switowski.com/blog/configuring-git)


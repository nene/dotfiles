-- Pull in the wezterm API
local wezterm = require("wezterm")

-- This will hold the configuration.
local config = wezterm.config_builder()

-- Window Config
-- config.initial_rows = 40
-- config.initial_cols = 40
config.enable_tab_bar = true
config.window_decorations = "RESIZE"
-- config.window_padding = {
--     left = "21%",
--     right = "1%",
--     top = 10,
--     bottom = 5,
-- }

config.window_background_opacity = 0.9

config.macos_window_background_blur = 10

config.default_cursor_style = "BlinkingBlock"

-- Theme Config
-- config.color_scheme = "Gruvbox Dark (Gogh)"
-- config.color_scheme = "Gruvbox Material (Gogh)"
-- config.color_scheme = "nightfox"
config.color_scheme = "Dracula"

-- Font Config
-- config.font = wezterm.font("Hack")
-- config.font = wezterm.font('JetBrains Mono', { weight = 'Bold', italic = true })
-- config.font = wezterm.font_with_fallback({ "Inconsolata Nerd Font Mono", "Hack" })
config.font = wezterm.font("FiraCode Nerd Font Mono", { weight = "Regular", stretch = "Normal", style = "Normal" })
-- config.font = wezterm.font("FiraCode Nerd Font Mono", {weight=450, stretch="Normal", style="Normal"})
config.font_size = 14
-- config.line_height = 1.2
-- config.command_palette_font_size = 16

-- Background Config
config.window_background_gradient = {
    orientation = "Vertical",
    colors = {
        "#223343",
        "#000000",
    },

    interpolation = "Linear",

    blend = "Rgb",
}

-- poon-hill.jpg
-- keyboards.jpg
-- bonny-star.jpg
-- sierra-mist.jpg
-- milkyway.jpg

local background_img = "bonny-star.jpg"

config.background = {
    {
        source = {
            File = wezterm.config_dir .. "/images/" .. background_img,
        },
        -- opacity = 0.3,
    },
    {
        source = {
            Color = "#000000",
        },
        opacity = 0.5,
    },
}

-- Keys
-- New Tab switch: CMD + SHIFT + [,]

config.keys = {
    {
        key = "f",
        mods = "CTRL|SHIFT",
        action = wezterm.action.ToggleFullScreen,
    },
}

-- finally, return the configuration to wezterm
return config

# Shells

- [Managing your dotfiles using `git`](https://medium.com/better-programming/managing-your-dotfiles-with-git-4dee603a19a2)


## ZSH
- [How to install and setup zsh in ubuntu 20.04](https://www.tecmint.com/install-zsh-in-ubuntu/)
- [How to install OHMYZSH in ubuntu 20.04](https://www.tecmint.com/install-oh-my-zsh-in-ubuntu/)
- [ohmyzsh plugins](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins)
- [ZSH-vi-mode](https://github.com/jeffreytse/zsh-vi-mode)


## Fish
- [Medium - why I use fish over bash and zsh](https://medium.com/better-programming/why-i-use-fish-shell-over-bash-and-zsh-407d23293839)
- [spacefish](https://github.com/matchai/spacefish/)



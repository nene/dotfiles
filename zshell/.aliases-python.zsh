
# Alias for python3

alias py3="/usr/bin/python3"
#alias python="/usr/bin/python3"
alias pym="python3 manage.py"
alias mkenv="python3 -m venv env"
alias startenv="source env/bin/activate && which python3"
alias stopenv="deactivate"

alias uuid="py3 -c 'import uuid, sys;sys.stdout.write(str(uuid.uuid4()))' "
# alias uuid="python -c 'import sys,uuid; sys.stdout.write(uuid.uuid4().hex)' | pbcopy && pbpaste && echo"


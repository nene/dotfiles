
## Admin Tasks
alias root="sudo -i"
alias su="sudo -i"
alias godmode="sudo -s"
alias reboot="sudo /sbin/reboot"
alias poweroff="sudo /sbin/poweroff"
alias halt="sudo /sbin/halt"
alias shutdown="sudo /sbin/shutdown"


# CPU Usage
alias cpu="top -o cpu"
alias mem="top -o rsize";

# clear the screen. Adopted from windows. I like it
alias cls="clear"

# create any non-existing (p)arent directories and explain (v) what was done
alias mkdir="mkdir -pv"

# explain (v) what was done when moving a file
alias mv="mv -v"

# copy contents of directories (r)ecursively and explain (v)
alias cp="cp -rv" 

alias rm="rm -i"

alias mv="mv -i"

alias grep="grep -i"

# Always try to (c)ontinue getting a partially-downloaded file
alias wget="wget -c"

# Alias for Alacritty with ligatures
#alias ala="$HOME/bin/alacritty"

# Youtube downloader
alias yt-dl="/usr/local/bin/yt-dlp_linux"

# Alias for vim
alias vi="/usr/bin/vim"

# Alias for neovim
alias nv="/usr/local/nvim-linux64/bin/nvim"

# Alias for neovide
alias neo="$HOME/bin/neovide --multigrid"

### Memory Consumption
alias df="df -H"
alias du="du -ch"
alias findbig5="find . -type f -exec ls -s {} \; | sort -n -r | head -5"

# du -hsx * | sort -rh | head -10
# du -hsx -- * | sort -rh | head -10


### Directory Usage

## List contents with (a)ll hidden files (including . and ..) (C)olumns, with class indicators (F)
#alias la="ls -aCF"
### List contents with size units (h). Reverse (r) sort (older entries first). Sort by (t)ime (most recently modified first)
#alias ll="ls -ltrh"
#alias l="ls -CF"
#alias l1="ls -1"

## Using LSD
alias ls="lsd"
alias l="ls -l"
alias la="ls -a"
alias lla="ls -la"
alias lt="ls --tree"


## quick jump to predefined directories 
alias dev="cd $HOME/Documents/Development/"
alias docs="cd $HOME/Documents/"
alias repos="cd $HOME/Documents/Repos/"
alias conf="cd $XDG_CONFIG_HOME"

alias wheather="curl wttr.in"

alias reload="source ~/.zshrc; echo shell config reloaded"


## tmux new -s (pwd | sed 's/.*\///g')


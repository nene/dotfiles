# ZShell

- [ZSH plugin manager - ohmyz.sh](https://ohmyz.sh/)
- [ZSH plugins](https://github.com/ohmyzsh/ohmyzsh/wiki/Plugins)
- [ZSH plugins](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins)

## Current Plugins

- z
- git
- vi-mode
- docker
- docker-compose
- [zsh-completions](https://github.com/zsh-users/zsh-completions)
- [zsh-autosuggestions](https://github.com/zsh-users/zsh-autosuggestions)
- zsh-syntax-highlighting

```
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
```

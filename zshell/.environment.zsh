## Environment Variables

export LANG=en_US.UTF-8

# for VIM and TMUX
# outside tmux or screen, `TERM` should be `xterm-256color`, but
# inside tmux or screen, it should be `screen-256color`.
if [ "$TERM" = "xterm" ]; then
    export TERM=xterm-256color
fi
if [ "$TERM" = "screen" -o "$TERM" = "screen-256color" ]; then
    export TERM=screen-256color
    unset TERMCAP
fi


export EDITOR="nvim"
export VISUAL=$EDITOR

export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}

## Neovim
export NVM_DIR=$XDG_CONFIG_HOME/nvim
export MYVIMRC=$XDG_CONFIG_HOME/nvim/init.lua


## Bat
export BAT_CONFIG_PATH=$XDG_CONFIG_HOME/bat/config

export MANPAGER="sh -c 'col -bx | bat -l man -p'"


## JAVA
# Java for macos
#export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_162.jdk/Contents/Home
#export JAVA_HOME=/Library/Java/JavaVirtualMachines/openjdk-11.0.2.jdk/Contents/Home

# Java for Linux
export JAVA8_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export JAVA11_HOME=/usr/lib/jvm/zulu11-jdk-linux_x64
export JAVA17_HOME=/usr/lib/jvm/zulu17-jdk-linux_x64
export JAVA21_HOME=/usr/lib/jvm/zulu21-jdk-linux_x64

# export JRE_HOME=$JAVA_HOME/jre
#
# export JAVA_HOME=$JAVA11_HOME
# export JAVA_HOME=$JAVA17_HOME
export JAVA_HOME=$JAVA21_HOME
export JDK_HOME=$JAVA_HOME
export JAVA=$JAVA_HOME/bin
export PATH=$JAVA:$PATH

## MAVEN
#export M2_HOME=/usr/local/apache-maven-3.6.1
#export M2=$M2_HOME/bin
#export MAVEN_HOME=/usr/local/apache-maven-3.6.1
#export MAVEN=$MAVEN_HOME/bin
#export PATH=$MAVEN:$PATH

## GOLANG
export GOROOT=/usr/local/go
export GOPATH=$HOME/Documents/Development/Golang/go_workspace
export GOBIN=$GOPATH/bin
export PATH=$GOROOT/bin:$PATH
export PATH=$GOPATH/bin:$PATH

## RUST
export PATH=$HOME/.cargo:$PATH

## Neovim
export NVIM=/usr/local/nvim-linux64
export PATH=$NVIM/bin:$PATH

## Treesitter
export PATH=$HOME/bin:$PATH

## LSP Servers Config
export LSP_SERVERS_HOME=$XDG_DATA_HOME/nvim/lsp_servers

## LSP for LUA
export SUMNEKO_LUA_HOME=$LSP_SERVERS_HOME/sumneko_lua
export SUMNEKO_LUA_SERVER=$SUMNEKO_LUA_HOME/extension/server
export PATH=$SUMNEKO_LUA_SERVER/bin:$PATH

#export PATH=$LSP_SERVERS_HOME/go:$PATH
## LSP for Python
export PATH=$HOME/.local/bin:$PATH

# For user tools
export PATH=$HOME/bin:$PATH



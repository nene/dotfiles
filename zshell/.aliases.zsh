
source ~/.aliases-fs.zsh
source ~/.aliases-fzf.zsh
source ~/.aliases-docker.zsh
source ~/.aliases-k8s.zsh
source ~/.aliases-python.zsh
source ~/.aliases-gcp.zsh
source ~/.aliases-git.zsh


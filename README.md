# My dotfiles Project

![dotfiles](dotfiles.png)

#### Always a WIP ;)

These are my configuration files for

- [Terminal Alacritty](https://github.com/alacritty/alacritty)
- [Alacritty with ligatures](https://dev.to/prakhil_tp/add-font-ligatures-to-your-alacritty-2fld)
- [add-font-ligatures-to-your-alacritty](https://dev.to/prakhil_tp/add-font-ligatures-to-your-alacritty-2fld)
- [neovim](https://neovim.io/)
- [tmux](https://github.com/tmux/tmux/wiki)

## Terminal

- [alacritty](https://github.com/alacritty/alacritty)
- [alacritty-theme changer](https://crates.io/crates/alacritty-theme)
- [alacritty color schemes](https://clcode.net/articles/color-schemes.md)
- [alacritty themes](https://github.com/eendroroy/alacritty-theme)

## ZSH

- [Z Shell](./zshell/README.md)

## Prompts

- [spaceship-prompt](https://github.com/denysdovhan/spaceship-prompt)
- [starship-prompt](https://starship.rs/)

## Neovim

- [Neovim Lua guide](https://github.com/nanotee/nvim-lua-guide)
- [Understanding vim mappings](https://medium.com/vim-drops/understand-vim-mappings-and-create-your-own-shortcuts-f52ee4a6b8ed)

### Copy between Linux and Neovim.

- Using Linux X11, you need to install xclip.
- Using Linux Wayland, you need to install wl-copy and wl-paste
- Using macOS, you need pbcopy

#### Neovim GUIs

- [Neovide](https://github.com/neovide/neovide)
- [Neovide guide docs](https://neovide.dev/)
- [VimR](https://github.com/qvacua/vimr)

## tmux

- [tmux wiki getting started](https://github.com/tmux/tmux/wiki/Getting-Started)
- [tmux cheatsheet.com](https://tmuxcheatsheet.com/)

## Pages

- [XDG Base directory specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html)
- [unix tools](unix-tools.md)
- [shells](shells.md)
- [tmux](tmux.md)
- [git links](git-links.md)
- [vim keymapings](https://alldrops.info/posts/vim-drops/2018-05-15_understand-vim-mappings-and-create-your-own-shortcuts/)
- [vim.fandom.com/wiki/Mapping keys in Vim - Tutorial Part1](<https://vim.fandom.com/wiki/Mapping_keys_in_Vim_-_Tutorial_(Part_1)>)
- [vim.fandom.com/wiki/Moving lines up or down](https://vim.fandom.com/wiki/Moving_lines_up_or_down)

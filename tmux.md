# TMUX


#### Create a tmux session
```
tmux new -s <session-name>
```

#### List tmux sessions
```
tmux ls

tmux list-sessions
```

#### Attach to an existing session
```
tmux attach -t <session-name>

tmux attach -t <session-index>
```

## Reference Links
- [tmux cheatsheet](tmux-cheatsheet.md)
- [tmux](https://github.com/tmux/tmux/wiki)
- [tmux wiki getting started](https://github.com/tmux/tmux/wiki/Getting-Started)
- [Guide to tmux Part1](https://www.hamvocke.com/blog/a-quick-and-easy-guide-to-tmux/)
- [Guide to tmux Part2](https://www.hamvocke.com/blog/a-guide-to-customizing-your-tmux-conf/)
- [tmuxcheatsheet](https://tmuxcheatsheet.com/)
- [tmux - CLI](https://towardsdatascience.com/command-line-interface-tmux-dabbd613ec2a)
- [tmux - complete tutorial](https://www.youtube.com/watch?v=Yl7NFenTgIo)
- [tmux - getting started](https://www.youtube.com/watch?v=252K9lrRdMU&t=383s)
- [GitHub repo](https://github.com/flipsidecreations/dotfiles/blob/master/.tmux.conf)
- [tmux - using sessions, windows, panes and buffers](https://www.youtube.com/watch?v=hbs7tuwpgZA&t=535s)
- [tmux book - Brian Hogan](https://pragprog.com/book/bhtmux/tmux)
- [tmux dracula color scheme how-to](https://cassidy.codes/blog/2019-08-03-tmux-colour-theme/)

- [`rothgar` awesome tmux](https://github.com/rothgar/awesome-tmux)
- [`square` maximum-awesome](https://github.com/square/maximum-awesome.git)
- [`gpakosz` great config](https://github.com/gpakosz/.tmux)
- [Github fenetikm/dotfile](https://github.com/fenetikm/dotfiles)
- [Tmux copy mode](https://github.com/gotbletu/shownotes/blob/master/tmux_2.4_copy_mode_vim.md)
- [video about tmux copy mode](https://www.youtube.com/watch?v=P8goLYv2vqs&t=9s)
- [good dotfile](https://gist.github.com/benawad/b768f5a5bbd92c8baabd363b7e79786f)
- [`brandur` tmux-extra](https://github.com/brandur/tmux-extra)

- [Medium - Command Line Interface - tmux](https://towardsdatascience.com/command-line-interface-tmux-dabbd613ec2a)


### Plugins
- [tpm - tmux plugin manager](https://github.com/tmux-plugins/tpm)
- [tmux-plugins](https://github.com/tmux-plugins)
- [better mouse mode](https://github.com/NHDaly/tmux-better-mouse-mode)
- [continuum](https://github.com/tmux-plugins/tmux-continuum)
- [enhanced tmux search](https://github.com/tmux-plugins/tmux-copycat)
- [vim plugin](https://github.com/tmux-plugins/vim-tmux)
- [resurrect - persists tmux environments across system restarts](https://github.com/tmux-plugins/tmux-resurrect)


### Themes
- [theme pack](https://github.com/jimeh/tmux-themepack)
- [nord](https://www.nordtheme.com/ports/tmux)

